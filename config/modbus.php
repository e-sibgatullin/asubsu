<?php

return [
    'line1' => [
        'host' => env('PLC_LINE1_IP'),
        'port' => env('PLC_LINE1_PORT'),
        'devAddr' => env('PLC_LINE1_DEV_ADDR')
    ],
    
    'line2' => [
        'host' => env('PLC_LINE2_IP'),
        'port' => env('PLC_LINE2_PORT'),
        'devAddr' => env('PLC_LINE2_DEV_ADDR')
    ]    
];