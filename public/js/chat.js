let socketChat = new WebSocket("ws://asubsu.local:6001/ws-chat")      
let textarea = document.getElementsByTagName('textarea')[0]
let userList = document.getElementById('myFormChat').firstElementChild;
let dstUser = '1111111111.1111111111'

window.onload = function(){
    document.getElementById('openChat').onclick = openForm;
    document.getElementById('closeChat').onclick = closeForm;
    document.getElementById('sendButton').onclick = sendMsg;
    userList.onclick = clickUser    
}

socketChat.onmessage = function(event){    
    responseHandle(event.data);
}

socketChat.onerror = function(error){
    console.log(error)
}

socketChat.onclose = function(event){
    console.log(event)
}

function openForm() {    
  document.getElementById('myFormChat').style.display = 'block'
}

function closeForm() {
  document.getElementById('myFormChat').style.display = 'none'
}

function sendMsg(){        
    msgReq = {
        msg : document.getElementById('msgText').value,
        dst: dstUser
    }
    
    textarea.value += 'вы: ' + msgReq.msg + '\n'
    socketChat.send(JSON.stringify(msgReq))
}

function responseHandle(responseData){
    data = JSON.parse(responseData)
    if(data.type == 0){
        
        while (userList.firstChild) {
            userList.removeChild(userList.firstChild);
        }
        
        for([index, userId] of data.msg.entries()){ 
            li = document.createElement('li')
            li.setAttribute('data-user-id', userId)
            li.innerHTML = `user_${index}`
            userList.append(li);
        }        
    }
    
    if(data.type == 2){
        textarea.value += data.src + ': ' + data.msg + '\n'
        dstUser = data.src
    }
}

function clickUser(){
    activeUser = event.target
    activeUser.classList.toggle('activeUserChat')  
    dstUser = activeUser.getAttribute('data-user-id')    
}
