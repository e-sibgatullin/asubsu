<?php
use BeyondCode\LaravelWebSockets\Facades\WebSocketsRouter;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {   
    if(auth()->check()){
        return redirect()->route('concreteBsu');
    }
    return redirect()->route('reportRecipe');
});

// Маршруты аутентификации...
Route::get('/login', 'AuthController@getLogin')->name('login');
Route::post('/login', 'AuthController@postLogin')->name('postLogin');
Route::get('/logout', 'AuthController@getLogout')->name('logOut');
Route::match(['get','post'], '/socialAuth/{socialName?}', 'AuthController@socialAuth')->name('socialAuth');
Route::match(['get','post'], '/socialAuthCallback', 'AuthController@socialAuthCallback')->name('socialAuthCallback');

// Маршруты регистрации...
Route::get('/register', 'AuthController@getRegister')->name('register');
Route::post('/register', 'AuthController@postRegister')->name('postRegister');

//Маршруты AJAX запросов
route::match(['get','post'],'/ajax/recipeDetail/{recipeID}','AjaxController@getRecipeDetail')->name('getRecipeDetail')->middleware('auth');


// маршруты АСУ
route::get('/concrete','ConcreteController@bsu')->name('concreteBsu')->middleware('auth');

// маршруты отчетов
Route::match(['get','post'], '/report/recipe', 'ReportController@recipe')->name('reportRecipe')->middleware('recipe');
Route::post('/report/recipe/xml', 'ReportController@recipeXML')->name('reportRecipeXML')->middleware('recipe');
Route::post('/report/recipe/{id}', 'ReportController@recipeDetail')->name('reportRecipeDetail')->middleware('recipe')->where('id', '[0-9]+');
Route::post('/report/recipe/{id}/viewRequest', 'ReportController@recipeViewRwquest')->name('reportRecipeViewRequest')->middleware('recipe')->where('id', '[0-9]+');
Route::match(['get','post'], '/report/claim', 'ReportController@claim')->name('reportClaim')->middleware('recipe');

// маршруты настроек АСУ
Route::get('/setings', 'SetingsController@index')->name('setings')->middleware('auth');
Route::get('/setings/device', 'SetingsController@device')->name('setingsDevice')->middleware('auth');
Route::get('/setings/plc', 'SetingsController@plc')->name('setingsPLC')->middleware('auth');
Route::get('/setings/comp', 'SetingsController@component')->name('setingsComp')->middleware('auth');
Route::get('/setings/recipe', 'SetingsController@recipe')->name('setingsRecipe')->middleware('auth');
Route::post('/setings/plc/add', 'SetingsController@plcAdd')->name('setingsPLCAdd')->middleware('auth');
Route::post('/setings/plc/addIO', 'SetingsController@plcIOAdd')->name('setingsPLCIOAdd')->middleware('auth');
Route::post('/setings/device/add', 'SetingsController@deviceAdd')->name('setingsDeviceAdd')->middleware('auth');
Route::post('/setings/comp/add', 'SetingsController@componentAdd')->name('setingsCompAdd')->middleware('auth');
Route::post('/setings/recipe/add', 'SetingsController@recipeAdd')->name('setingsRecipeAdd')->middleware('auth');

//Маршруты пропарки
route::match(['get','post'],'/steaming','SteamingController@index')->name('steaming')->middleware('auth');
route::match(['get','post'],'/steaming/getSteamData','SteamingController@getSteamData')->name('getSteamData')->middleware('auth');
route::match(['get','post'],'/steaming/switchSteam','SteamingController@switchSteam')->name('switchSteam')->middleware('auth');

// Маршруты вэбсокетов
WebSocketsRouter::webSocket('/ws-chat', \App\Custom\WSChat::class);
WebSocketsRouter::webSocket('/ws-concrete', \App\Custom\WSConcrete::class);
