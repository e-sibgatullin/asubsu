<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::get('/v1/recipe/{id?}', 'API\v1\RecipeController@getRecipe')->where('id', '[0-9]+');
Route::post('/v1/recipe', 'API\v1\RecipeController@addRecipe');
Route::delete('/v1/recipe/{id}', 'API\v1\RecipeController@delRecipe')->where('id', '[0-9]+');