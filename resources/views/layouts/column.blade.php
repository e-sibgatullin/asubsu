@extends('layouts.main')
@section('container')
<div id="receipt_detail" class="receipt hide"></div>
<div class="left">                    
    @yield('left_column')                    
</div>
<div class="right">
    @yield('right_column')    
</div>
@endsection