<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ЖБИ-2 {{ $title ?? '' }}</title>        
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>        
        <link rel="stylesheet" href="{{ url('css/style.css') }}" />                
    </head>
    <body>
        <header>
            <nav>
                <ul class="topmenu">
                    @if (Auth::check())
                    <li><a href="{{ route('concreteBsu') }}">АСУ-БСУ</a></li>
                    <li><a href="{{ route('setings') }}">Настройки БСУ</a></li>
                    <li><a href="{{ route('steaming') }}">Пропарка</a></li>
                    @endif        
                    <li><a href="{{ route('reportRecipe') }}">Отчет по рецептам</a></li>
                    <li><a href="{{ route('reportClaim') }}">Отчет по заявкам</a></li>               
                </ul>    
                <ul class="topmenu">
                    @if (Auth::check())
                    <li><a href="{{ route('logOut') }}">Выход</a></li>
                    @else
                    <li><a href="{{ route('login') }}">Вход</a></li>
                    @endif
                </ul>
            </nav>
            @yield('header') 
        </header>
        <section>
            <div class="container">  
                <div id='notice_box'>                    
                </div>                
                @yield('container')  
                @include('chat')
            </div>
        </section>
        <footer>

        </footer>
        <span ID = "toTop" ><span ID = "toTopB" ></span></span>    
        <script src="{{ url('js/app.js') }}"></script>                  
        <script src="{{ url('js/chat.js') }}"></script>                  
    </body>
</html>
