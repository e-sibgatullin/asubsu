<div class="device" id="mixer_{{ $device->id }}">
    <p>{{ $device->name }}</p>    
    <span class="regVol">0</span>
    <input type="checkbox" class="bsu_open_checkbox" id="checkbox_mixer_{{ $device->id }}">
    <label for="checkbox_mixer_{{ $device->id }}"><div id="tick_mark"></div></label>    
    <object id="do_24" type="image/svg+xml" data="{{ url('img/mixer.svg') }}"></object>
</div>

