<div class="device" id="dispenser_{{ $device->id }}">    
    <p>{{ $device->name }}</p>
    <span class="regVol">0</span>
    <input type="checkbox" class="bsu_open_checkbox" id="checkbox_dispenser_{{ $device->id }}">
    <label for="checkbox_dispenser_{{ $device->id }}"><div id="tick_mark"></div></label>
    <object data="img/{{ $device->img }}.svg" type="image/svg+xml"></object>    
</div>