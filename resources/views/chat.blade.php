<div class="open-button" id="openChat">Чат</div>

<div class="chat-popup" id="myFormChat">
    <ul>

    </ul>
    <div class="form-container">    
        <label for="msg"><b>Сообщение</b></label>
        <textarea name="msg" ></textarea>
        <input id="msgText" type="text" placeholder="Введите сообщение.." required>
        <div id="sendButton" class="btn">Отправить</div>
        <div class="btn" id="closeChat">Закрыть</div>
    </div>
</div>