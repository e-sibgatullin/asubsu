@extends('layouts.column')
@section('left_column')
<div class="admin_left">
    <div class="admin_menu">
        <a href="{{ route('setingsPLC') }}">Контролеры</a>
    </div>
    <div class="admin_menu">
        <a href="{{ route('setingsDevice') }}">Устроиства</a>
    </div>
    <div class="admin_menu">
        <a href="{{ route('setingsComp') }}">Компоненты</a>
    </div>
    <div class="admin_menu">
        <a href="{{ route('setingsRecipe') }}">Рецепты</a>
    </div>    
</div>
@endsection
@section('right_column')
@yield('data')
@endsection