@extends('setings.main')
@section('data')
    <h1>Контролеры</h1>
    <hr>
    <div class="form_setings">
        <div>
            <h1>Добавить контроллер</h1>
            @include('forms.addPLC')
        </div>        
        <div id="plc_io" class="hide">
            <h1>Добавить входы/выходы</h1>
            @include('forms.addPLCIO')
        </div>
    </div>    
    <div class="view_setings">
        <h1>Добавленные контроллеры</h1>            
        @foreach($plc as $value)
        <div class="view_plc">
            <div class="plc">
                <input type="hidden" value="{{ $value->id }}">
                <span>{{ $value->name }}</span>
                <span>{{ $value->ip }}</span>   
            </div>
            @foreach($value->plc_io as $io)
                <div class="io">           
                    <span>{{ $io->io_numb }}</span>                
                    <span>{{ $io->io_type_data }}</span>                
                    <span>{{ $io->io_fc }}</span>                            
                </div>
            @endforeach            
        </div>        
        @endforeach
    </div>
@endsection