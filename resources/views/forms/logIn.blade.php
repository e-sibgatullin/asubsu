@extends('layouts.main')
@section('container')
<form method="POST" action="{{ route('login') }}" class="logIn">
      {!! csrf_field() !!}

      <div class="@error('login') valid-error @enderror">
        <p>Логин</p>
        <input type="text" name="login" value="{{ old('login') ?? '' }}">
        @error('login')
            <p>{{ $message }}</p>
        @enderror
      </div>

      <div  class="@error('password') valid-error @enderror">
        <p>Пароль</p>
        <input type="password" name="password" id="password">
        @error('password')
            <p>{{ $message }}</p>
        @enderror
      </div>
      
      <!-- <div>
        <p>Запомнить меня</p>
        <input type="checkbox" name="remember">
      </div>
      -->
      
      <div class="button_login_group">
          <div>
              <button type="submit" title="Войти">&#xf09c</button>
          </div>
          <div>
              <button type="submit" formaction="{{ route('register') }}" title="Зарегистрироваться">&#xf234</button>
          </div>  
          <div>
              <button type="submit" formaction="{{ route('socialAuth', ['socialName'=>'vkontakte']) }}" title="Войти через Вконтакт">&#xf189</button>
          </div>
      </div>
      
    </form>
@endsection