@extends('layouts.main')
@section('container')
    <form method="POST" action="{{ route('postRegister') }}">
      {!! csrf_field() !!}

      <div>
        Логин
        <input type="text" name="name" value="{{ old('name') }}">
      </div>  

      <div>
        Пароль
            <input type="password" name="password">
      </div>

      <div>
        Потверждение пороля
        <input type="password" name="password_confirmation">
      </div>

      <div>
        <button type="submit">Зарегистрировать</button>
      </div>
    </form>
@endsection