<div id='wrapper'>
    <h1>Параметры отчета</h1>
<form id="options_report" name="paramReport" method="POST" autocomplete="off" action="">     
    {{ csrf_field() }}    
    <h3>Период</h3>
    <div><span class="spanFix30 ">С:</span><input class="{{ $errors->has('dt_s') ? 'inputError' : '' }}" name="dt_s" id="dt_s" type="datetime-local" placeholder="период начало" value="{{ $dt_s }}"></div>        
    <div><span class="spanFix30">по:</span><input class="{{ $errors->has('dt_e') ? 'inputError' : '' }}" name="dt_e" id="dt_e" type="datetime-local" placeholder="период конец" value="{{ $dt_e }}"></div>        
    <h3>Линия БСЦ</h3>
    <div>
        <span class="spanFix30"></span>
        <select class="{{ $errors->has('lines') ? 'inputError' : '' }}" name="lines" type="select" id="line">
            <option value="lineOne" @if($lines == 'lineOne') selected @endif>линия 1</option> 
            <option value="lineTwo" @if($lines == 'lineTwo') selected @endif>линия 2</option> 
        </select>
    </div>        
    <div class="button_group">
        <button name="create" type="submit"></button>                    
        <button name="export" type="submit" formaction="{{ route('reportRecipeXML') }}"></button>                
    </div>
</form>
</div>