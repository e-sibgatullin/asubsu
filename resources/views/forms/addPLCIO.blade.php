<form method="POST" action="{{ route('setingsPLCIOAdd') }}">
    <input id="plc_id" name="plc_id" type="hidden" value="">
    {!! csrf_field() !!}
    <div>           
        <div class="@error('io_numb') valid-error @enderror">
            <input id="plc_io_numb" type="number" name="io_numb" placeholder="Номер регистра" value="{{ old('io_numb') ?? '' }}">
            @error('io_numb')
            <p>{{ $message }}</p>
            @enderror
        </div>
        <div class="@error('io_data_type') valid-error @enderror">
            <input id="plc_io_data_type" type="number" name="io_data_type" placeholder="Тип данных регистра" value="{{ old('io_data_type') ?? '' }}">
            @error('io_data_type')
            <p>{{ $message }}</p>
            @enderror
        </div>
        <div class="@error('io_fc') valid-error @enderror">
            <input id="plc_io_fc" type="number" name="io_fc" placeholder="Пространсво регистра" value="{{ old('io_fc') ?? '' }}"> 
            @error('io_fc')
            <p>{{ $message }}</p>
            @enderror
        </div>        
        <button id="plc_io_add" type="submit" formaction="{{ route('setingsPLCIOAdd') }}">&#xf0c7</button>
    </div>
</form>