<form method="POST" action="{{ route('setingsPLCAdd') }}">
    {!! csrf_field() !!}
    <div>        
        <div class="@error('name') valid-error @enderror">
            <input id="plc_name" name="name" placeholder="Имя контролера" value="{{ old('name') ?? '' }}">
            @error('name')
            <p>{{ $message }}</p>
            @enderror
        </div>
        <div class="@error('ip') valid-error @enderror">
            <input id="plc_ip" name="ip" placeholder="Сетевой адрес" value="{{ old('ip') ?? '' }}">
            @error('ip')
            <p>{{ $message }}</p>
            @enderror
        </div>
        <div class="@error('line') valid-error @enderror">
            <input id="plc_line" name="line" placeholder="производственная линия" value="{{ old('line') ?? '' }}"> 
            @error('line')
            <p>{{ $message }}</p>
            @enderror
        </div>        
        <button id="plc_add" type="submit" formaction="{{ route('setingsPLCAdd') }}">&#xf0c7</button>
    </div>
</form>