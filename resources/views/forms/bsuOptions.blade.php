<div id='wrapper'>
    <h1>Параметры БСУ</h1>
<form id="options_report" name="bsuOptions" method="POST" autocomplete="off" action="">     
    {{ csrf_field() }}    
    <h3>Рецепты</h3>         
    <select id="bsu_recipe_select" class="live_search_select" name="bsu_recipe_select" type="select" id="line">
        @foreach($recipes as $recipe)
            <option value="{{ $recipe->id }}">{{ $recipe->shortName }}</option> 
        @endForeach            
    </select>    
    <h3>Количество кубов</h3>
    <div class="bsu_quantity">
        <input type="number" name="concreteQuantity">
    </div>
    <div class="button_group">
        <button id="start" name="makeConcrete" type="button"></button>                    
        <button id="addQueue" name="addQueueConcrete" type="button"></button>                
        <button id="reconnModbus" name="reconnModbus" type="button">&#xf1e6</button>
    </div>
    <h3>Компоненты</h3>
    <div class="bsu_components">
        <div><p>Наименование</p><p>Количество</p></div>
    </div>     
    
    <h3>Очередь</h3>
    <div class="bsu_queue">
        <div><p>Рецепт</p><p>Количество куб.м.</p></div>
    </div>
</form>
</div>