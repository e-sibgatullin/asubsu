@extends('layouts.column')
@section('left_column')    
    @include('forms.bsuOptions')
@endsection
@section('right_column')  
@foreach($devices as $key => $device)
    <div class="{{ $key }}s">
        @each("devices.$key", $device, "device")
    </div>
@endforeach

@endsection
