@extends('layouts.main')
@section('container')
<div id="lineChart" class="lineChart"></div>
@if(is_array($facilitys) and $facilitys['status'] == 'error')
    @component('alert')
    <strong>{{ $facilitys['message'] }}</strong>
    @endcomponent    
@else
    @foreach($facilitys as $name => $facility)
        <div id="{{ $name }}" class="steam">
            <div id="error" class="plc_error @if($facility->status == 'error') hide @endif"><p> @if($facility->status == 'error') {{ $facility->msg }} @endif</p></div>
            <p>{{ $name }}</p>
            <div id="{{ $name }}_switch" class="steam_switch">
            <input id="{{ $name }}_steam" name="{{ $name }}" type="checkbox" @if($facility->msg[3] == true) checked @endif>
            <label for="{{ $name }}_steam"></label>        
            </div>
            <div id="{{ $name }}_chart"></div>    
        </div>
    @endforeach
@endif

@endsection