<div id="detail">                    
        <div class="receipt_header">            
            <span id="close" class="close"></span>
            <input id="hiddenLine" type="hidden" name="line" value="{{ $data['lines'] ?? '' }}">
            <input id="hiddenRecID" type="hidden" name="receiptID" value="{{ $recipe->id ?? '' }}">
            <input id="hiddenClID" type="hidden" name="clientID" value="{{ $data['clientID'] ?? '' }}">
            <p>Рецепт № <span>{{ $recipe->number ?? '' }}</span></p>
            <p>Название рецепта: <span>{{ $recipe->shortName ?? '' }}</span></p>
            <p>группа рецепта: <span>{{ $recipe->group ?? '' }}</span></p>                
        </div>                
            <div class="receipt_comp">                    
            @foreach($recipe->components as $comp)            
                <div>
                    <span>{{ $comp->shortName }}</span>
                    <input name="comp_{{ $comp->id }}" type="number" value="{{ $comp->pivot->m }}">
                </div>
            @endforeach                                         
            <a id="viewRequest" class="request_header" href="{{ route('reportRecipeViewRequest',['id'=>$recipe->id]) }}">Показать заявки</a><label id="rolUpDownRequest" for="viewRequest" class="roll_down"></label>                                                
        </div>         
        <div class="request" id="recipeClaim"></div>
</div>