<div>
    <div id="print" data-toggle="request" class="print">&#xf02f</div>
</div>
<div id="recipeClaim">        
    <div id="table_request_header" class="table_request_header">
        <div>№ заявки</div>
        <div>Дата заявки</div>
        <div>кол. куб. по заявке</div>
        <div>кол. куб. по факту</div>
    </div>        
    @foreach($requests as $request)   
    
    <div id="row_request" class="row_request">         
        <div>{{ $request->request }}</div>
        <div>{{ $request->orData }}</div>
        <div>{{ $request->orQuantity }}</div>
        <div>{{ $request->factQuantity }}</div>
    </div>    
    @endforeach
</div>

