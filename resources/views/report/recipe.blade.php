@extends('layouts.column')
@section('left_column')
@include('forms.reportOptions')
@endsection
@section('right_column')
<div id="section-to-print">
<h1> АО Завод ЖБИ-2 </h1>
<h2>Отчет по производству бетонных смесей за период с: {{ $dt_s ?? '' }} по: {{ $dt_e ?? '' }}</h2>
<table border="1" class="recipe_table">
    <thead>
        <tr>
            <th rowspan="2"></th>
            <th rowspan="2" class="th50"><span>№ рецепта</span></th>
            <th rowspan="2" class="th50"><span>Марка</span></th>
            <th rowspan="2" class="th50"><span>Кол-во, м.куб.</span></th>
            <th colspan="3"><span>Цемент марки, тн</span></th>                            
            <th colspan="3"><span>Песок, тн</span></th>                            
            <th colspan="3"><span>Фракция 5-20, тн</span></th>                            
            <th colspan="3"><Span>Фракция 20-40, тн</span></th>                            
            <th rowspan="2"><div class="Tvertical"><span><span>ПАЩ, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>МРЗ, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>С3, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>SIKA 5600, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>ДОБАВКА-5, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>ДОБАВКА-4, кг</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>ВОДА, тн</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>Известь, тн</span></span></div></th>
            <th rowspan="2"><div class="Tvertical"><span><span>Керамзит, тн</span></span></div></th>
        </tr>
        <tr>                            
            <th><span>400</span></th>                            
            <th><span>500</span></th>                            
            <th><span>600</span></th>                            
            <th><div class="Tvertical"><span><span>Горный</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Речной</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Тамбов</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Щебень</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Гравий</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Шлак</span></span></div></th>                                                        
            <th><div class="Tvertical"><span><span>Щебень</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Гравий</span></span></div></th>                            
            <th><div class="Tvertical"><span><span>Шлак</span></span></div></th>                                                        
        </tr>
    </thead>                            
    @isset ($arrayData)    
    <tbody>        
        @foreach ($arrayData as $line)            
            @if ($loop->first)
                <tr>
                    <td colspan="25">{{ $line->clientName }}</span></td>                            
                    </tr>
                <tr>        
                    <td colspan="25"> {{ $line->receiptGroup }}</span></td>                            
                </tr>
            @elseif(!$loop->last)
                @if($arrayData[$loop->index-1]->clientName <> $line->clientName)
                <tr>
                    <td colspan="25">{{ $line->clientName }}</span></td>                            
                </tr>
                @endif
                @if($arrayData[$loop->index-1]->receiptGroup <> $line->receiptGroup)
                <tr>
                    <td colspan="25">{{ $line->receiptGroup }}</span></td>                            
                </tr>
                @endif
            @endif
        @if(!$loop->last)
        <tr>
            <td><span>Заказ</span></td>
            <td rowspan="2"><input type="hidden" name="clientID" value=" {{ $line->clientID }}"><a title="Детализация по рецепту" data-toggle="receipt" class="receipt_link" href=" {{ route('reportRecipeDetail',['id'=>$line->receipt]) }}"> {{ $line->receiptNumber }}</a></td>
            <td rowspan="2"> {{ $line->receiptBrand }}</td>
            <td><span>{{ $line->rq_volume }}</span></td>
            <td><span>{{ $line->M400_SP }}</span></td>
            <td><span>{{ $line->M500_SP }}</span></td>
            <td><span>{{ $line->M600_SP }}</span></td>
            <td><span>{{ $line->MS_SP }}</span></td>
            <td><span>{{ $line->RS_SP }}</span></td>
            <td><span>{{ $line->TS_SP  }}</span></td>
            <td><span>{{ $line->CS1_SP }}</span></td>
            <td><span>{{ $line->CS2_SP }}</span></td>
            <td><span>{{ $line->CS3_SP }}</span></td>
            <td><span>{{ $line->CS4_SP }}</span></td>
            <td><span>{{ $line->CS5_SP }}</span></td>
            <td><span>{{ $line->CS6_SP }}</span></td>
            <td><span>{{ $line->A1_SP }}</span></td>
            <td><span>{{ $line->A2_SP }}</span></td>
            <td><span>{{ $line->A3_SP }}</span></td>
            <td><span>{{ $line->SIKA5600_SP }}</span></td>
            <td><span>{{ $line->M550_SP }}</span></td>
            <td><span>{{ $line->A4_SP }}</span></td>
            <td><span>{{ $line->W_SP }}</span></td>
            <td><span>{{ $line->A5_SP }}</span></td>
            <td><span>{{ $line->CLAYITE_SP }}</span></td>
        </tr>
        <tr>
            <td><span>Факт</span></td>                            
            <td><span>{{ $line->tt_volume }}</span></td>
            <td><span>{{ $line->M400_PV }}</span></td>
            <td><span>{{ $line->M500_PV }}</span></td>
            <td><span>{{ $line->M600_PV }}</span></td>
            <td><span>{{ $line->MS_PV }}</span></td>
            <td><span>{{ $line->RS_PV }}</span></td>
            <td><span>{{ $line->TS_PV }}</span></td>
            <td><span>{{ $line->CS1_PV }}</span></td>
            <td><span>{{ $line->CS2_PV }}</span></td>
            <td><span>{{ $line->CS3_PV }}</span></td>
            <td><span>{{ $line->CS4_PV }}</span></td>
            <td><span>{{ $line->CS5_PV }}</span></td>
            <td><span>{{ $line->CS6_PV }}</span></td>
            <td><span>{{ $line->A1_PV }}</span></td>
            <td><span>{{ $line->A2_PV }}</span></td>
            <td><span>{{ $line->A3_PV }}</span></td>
            <td><span>{{ $line->SIKA5600_PV }}</span></td>
            <td><span>{{ $line->M550_PV }}</span></td>
            <td><span>{{ $line->A4_PV }}</span></td>
            <td><span>{{ $line->W_PV }}</span></td>
            <td><span>{{ $line->A5_PV }}</span></td>
            <td><span>{{ $line->CLAYITE_PV }}</span></td>
        </tr>
        @if(array_key_exists('total_receiptBrand', $line))
        <tr class="total_brand">
            <td rowspan="2" colspan="3" >Итого по марке</td>                            
            <td><span>{{ $line->total_receiptBrand['rq_volume'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M400_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M500_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M600_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['MS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['RS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['TS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS1_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS2_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS3_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS4_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS5_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS6_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A1_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A2_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A3_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['SIKA5600_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M550_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A4_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['W_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A5_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CLAYITE_SP'] }}</span></td>
        </tr>
        <tr class="total_brand">                                                        
            <td><span>{{ $line->total_receiptBrand['tt_volume'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M400_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M500_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M600_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['MS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['RS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['TS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS1_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS2_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS3_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS4_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS5_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CS6_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A1_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A2_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A3_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['SIKA5600_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['M550_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A4_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['W_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['A5_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptBrand['CLAYITE_SP'] }}</span></td>
        </tr>
        @endif
        @if(array_key_exists('total_receiptGroup', $line))
        <tr class="total_group">
            <td rowspan="2" colspan="3">Итого по группе</td>                            
            <td><span>{{ $line->total_receiptGroup['rq_volume'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M400_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M500_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M600_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['MS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['RS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['TS_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS1_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS2_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS3_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS4_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS5_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS6_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A1_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A2_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A3_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['SIKA5600_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M550_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A4_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['W_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A5_SP'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CLAYITE_SP'] }}</span></td>
        </tr>
        <tr class="total_group">                                                        
            <td><span>{{ $line->total_receiptGroup['tt_volume'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M400_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M500_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M600_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['MS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['RS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['TS_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS1_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS2_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS3_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS4_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS5_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CS6_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A1_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A2_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A3_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['SIKA5600_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['M550_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A4_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['W_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['A5_PV'] }}</span></td>
            <td><span>{{ $line->total_receiptGroup['CLAYITE_SP'] }}</span></td>
        </tr>
        @endif
        @if(array_key_exists('total_clientName', $line))
        <tr class="total_client">
            <td rowspan="2" colspan="3">Всего по клиенту</td>                            
            <td><span>{{ $line->total_clientName['rq_volume'] }}</span></td>
            <td><span>{{ $line->total_clientName['M400_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['M500_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['M600_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['MS_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['RS_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['TS_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS1_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS2_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS3_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS4_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS5_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS6_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['A1_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['A2_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['A3_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['SIKA5600_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['M550_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['A4_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['W_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['A5_SP'] }}</span></td>
            <td><span>{{ $line->total_clientName['CLAYITE_SP'] }}</span></td>
        </tr>
        <tr class="total_client">                                                        
            <td><span>{{ $line->total_clientName['tt_volume'] }}</span></td>
            <td><span>{{ $line->total_clientName['M400_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['M500_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['M600_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['MS_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['RS_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['TS_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS1_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS2_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS3_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS4_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS5_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CS6_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['A1_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['A2_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['A3_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['SIKA5600_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['M550_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['A4_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['W_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['A5_PV'] }}</span></td>
            <td><span>{{ $line->total_clientName['CLAYITE_SP'] }}</span></td>
        </tr>
        @endif
        @endif
        @if($loop->last)
        <tr class="total">
            <td rowspan="2" colspan="3">Общий итог</td>                            
            <td><span>{{ $arrayData['total']['rq_volume'] }}</span></td>
            <td><span>{{ $arrayData['total']['M400_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['M500_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['M600_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['MS_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['RS_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['TS_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS1_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS2_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS3_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS4_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS5_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CS6_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['A1_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['A2_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['A3_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['SIKA5600_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['M550_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['A4_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['W_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['A5_SP'] }}</span></td>
            <td><span>{{ $arrayData['total']['CLAYITE_SP'] }}</span></td>
        </tr>
        <tr class="total">                            
            <td><span>{{ $line['tt_volume'] }}</span></td>
            <td><span>{{ $line['M400_PV'] }}</span></td>
            <td><span>{{ $line['M500_PV'] }}</span></td>
            <td><span>{{ $line['M600_PV'] }}</span></td>
            <td><span>{{ $line['MS_PV'] }}</span></td>
            <td><span>{{ $line['RS_PV'] }}</span></td>
            <td><span>{{ $line['TS_PV'] }}</span></td>
            <td><span>{{ $line['CS1_PV'] }}</span></td>
            <td><span>{{ $line['CS2_PV'] }}</span></td>
            <td><span>{{ $line['CS3_PV'] }}</span></td>
            <td><span>{{ $line['CS4_PV'] }}</span></td>
            <td><span>{{ $line['CS5_PV'] }}</span></td>
            <td><span>{{ $line['CS6_PV'] }}</span></td>
            <td><span>{{ $line['A1_PV'] }}</span></td>
            <td><span>{{ $line['A2_PV'] }}</span></td>
            <td><span>{{ $line['A3_PV'] }}</span></td>
            <td><span>{{ $line['SIKA5600_PV'] }}</span></td>
            <td><span>{{ $line['M550_PV'] }}</span></td>
            <td><span>{{ $line['A4_PV'] }}</span></td>
            <td><span>{{ $line['W_PV'] }}</span></td>
            <td><span>{{ $line['A5_PV'] }}</span></td>
            <td><span>{{ $line['CLAYITE_PV'] }}</span></td>
        </tr> 
        @endif 
        @endforeach
    </tbody>                                            
    @endisset
</table>
</div>
@endsection