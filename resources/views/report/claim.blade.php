@extends('layouts.column')
@section('left_column')
@include('forms.reportOptions')
@endsection
@section('right_column')
<div id="section-to-print">
    <h1>АО Завод ЖБИ-2</h1>
    <h2>Отчет по заявкам: {{ $lines ?? '' }} линия детально по заявкам с: {{ $dt_s ?? '' }} по: {{ $dt_e ?? '' }}</h2>
    <table class="reportReq">
        <thead>
            <tr>
                <th>Заявка</th>
                <th>Время выгрузки</th>
                <th>Марка</th>
                <th>Рецепт</th>
                <th>Заказчик</th>
                <th>Объём</th>
                <th>Автомобиль</th>
                <th>Мешалка</th>
            </tr>
        </thead>        

        <tbody>
            @foreach($claims as $claim)
            <tr>
                <td>{{ $claim->id }}</td>
                <td>{{ $claim->started }}</td>
                <td>{{ $claim->recipe->brand }}</td>
                <td>{{ $claim->recipe->shortName }}</td>
                <td>{{ $claim->client->shortName }}</td>
                <td>{{ $claim->volume }}</td>
                <td>{{ $claim->machine }}</td>
                <td>{{ $claim->mixer }}</td>
            </tr>
            @endforeach
        </tbody>

        </tbody>    
    </table>
</div>
@endsection
