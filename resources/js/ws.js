if (location.pathname.includes('concrete')) {
    let socket = new WebSocket("ws://asubsu.local:6001/ws-concrete")      
    let wsStart = document.getElementById('start')
    let wsAddQueue = document.getElementById('addQueue')
    let bsuOptionForm = document.forms.bsuOptions
    let checkbox = document.querySelectorAll('.bsu_open_checkbox')
    let reconnModbus = document.getElementById('reconnModbus')
    let queue = document.querySelector('.bsu_queue')
    let sendData = []

    for (openButton of checkbox) {
        openButton.onclick = clickCheckBox;
    }    
    
    setInterval(sendMsg);
    
    function clickCheckBox() {
        devInfo = this.parentNode.id.split('_', 2)
        addSendData('manualOpen', devInfo[0], devInfo[1], event.target.checked)        
    }

    socket.onmessage = function (event) {
        if (event.data == '') {
            return false
        }

        responseData = JSON.parse(event.data)
        
        showResponse(responseData.data)
        showQueue(responseData.queue)
        
        if (responseData.state == 'error') {
            wsStart.disabled = false
            return
        }

        if(sendData.length === 0){
            addSendData('make')        
        }
    }

    socket.onerror = function (error) {
        console.log("Ошибка " + error.message)
    }

    socket.onclose = function (event) {
        if (event.wasClean) {
            console.log('Соединение закрыто чисто')
        } else {
            console.log('Обрыв соединения')
        }
        console.log('Код: ' + event.code + ' причина: ' + event.reason)
    }

    wsStart.onclick = function () {
        if (bsuOptionForm.concreteQuantity.value == "") {
            throw_message("не заполнено поле количество")
            return
        }

        this.disabled = true
        addSendData('make', 1, bsuOptionForm.bsu_recipe_select.value, bsuOptionForm.concreteQuantity.value)        
    }

    wsAddQueue.onclick = function () {
        if (bsuOptionForm.concreteQuantity.value == "") {
            throw_message("не заполнено поле количество")
            return
        }

        addSendData('addQueue', bsuOptionForm.bsu_recipe_select.value, bsuOptionForm.concreteQuantity.value)
    }

    reconnModbus.onclick = function () {
        addSendData('reconnModbus')
    }

    function showResponse(data) {           
        if (typeof data == 'string') {            
            throw_message(data)
            return
        }

        for (dev of data) {   
            if('hopper' in dev){
                hopper = document.getElementById('checkbox_hopper_' + dev.hopper.id)
                hopper.checked = dev.hopper.state == 1 ? true : false            
            }
            
            if('dispenser' in dev){
                dispenser = document.getElementById('dispenser_' + dev.dispenser.id)            
                dispenserSpan = dispenser.querySelector('.regVol')
                dispenserCheckbox = dispenser.querySelector('.bsu_open_checkbox')
                dispenserSpan.innerHTML = dev.dispenser.currentMass.toFixed(2)
                dispenserCheckbox.checked = dev.dispenser.openPort == 1 ? true : false
            }            
        } 
    }

    function showQueue(data) {
        if (data === undefined) {
            return
        }

        let clientCountQueue = queue.childElementCount;
        let serverCountQueue = data.length + 1;

        if (clientCountQueue !== serverCountQueue) {
            handleQueue(data, clientCountQueue, serverCountQueue)
        }

    }

    function handleQueue(data, clientCountQueue, serverCountQueue) {        
        if (clientCountQueue < serverCountQueue) {
            recipe =  data.pop();
            recipeNumb = Object.keys(recipe).pop();
            quantity = Object.values(recipe).pop();
            elem = document.createElement('div')
            elem.innerHTML = `<p>${getRecipeNameForId(recipeNumb)}</p><p>${quantity}</p>`
            queue.append(elem)
        }

        if (clientCountQueue > serverCountQueue) {
            queue.children[1].remove();            
        }                
        
    }

    function sendMsg() {          
        if(sendData.length == 0){
            return
        }        
        
        socket.send(JSON.stringify(sendData.pop()))
    }

    function addSendData(method, ...properties) {
        dataObj = {
            properties,
            method
        }        
        
        sendData.push(dataObj)        
    }
    
    function getRecipeNameForId(recipeId){        
        for(recipe of document.getElementById('bsu_recipe_select').children){
            if(recipe.getAttribute('value') == recipeId){
                return recipe.text
            }
        }           
    }    
}

function throw_message(str) 
{
    elem = document.createElement('p')    
    elem.id = 'notice_msg'   
    elem.innerHTML = str  
    
    $("#notice_box").append(elem);
    $(elem).fadeIn(500)
            .delay(3000)
            .fadeOut(500)
            .delay(100)
                .queue(function() {
                    $(this).remove();
                });
}