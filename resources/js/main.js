var timerModbus;
var ajaxReq = true;
var msg = "Запрос AJAX выполнелся не удачно: "; 
var device = [];
var steamingDevice = {};
var steamChartData;
var steamChartOptions;
var steamChart = {};

jQuery(function () {    
    $(":checkbox").each(function(){
        reg = $(this).attr('id').slice($(this).attr('id').lastIndexOf('_')+1);
        device[reg] = 0;
    });
    
    $("#error_msg").on('click', function(){        
        $(this).toggleClass('error_push');        
    });
    
    $("#error_msg").on('click', 'p', function(){
        $(this).fadeOut(500).remove(); 
        return false;
    });
    
    if (location.pathname.includes('steaming')){         
        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(steaming);        
    }
    
    $('#stopBach').hide();    
    $("#receiptSelect").hide();
    $(document).ajaxStart(function () {
        $('.ajaxLoadfullPage').show();
    });
    $(document).ajaxStop(function () {
        $('.ajaxLoadfullPage').hide();
    });
    $(document).ajaxError(function () {
        $('.ajaxLoadfullPage').hide();
    });
    
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $('body,html').animate({scrollTop: 0}, 800);
    });

    $(":checkbox").change(function(){
        reg = $(this).attr('id').slice($(this).attr('id').lastIndexOf('_')+1);        
        if(this.checked){
            device[reg] = 1;            
        } else {
            device[reg] = 0;            
        }        
    });    
    
    $(".button_group").on('click', 'li', function(){        
        $('#receiptSelect').val($(this).attr("id"));        
        $("#searchReceipt").val($(this).text());
        $("#receiptSelect").trigger('change')
    });    

    $("#bsu_recipe_select").on('change',function(){        
        url = '/ajax/recipeDetail/' + $(this).val();                
        ajaxQuery(url,"",concreteRecipeDetail);
    });    

    $(document).on('click', '[data-toggle=receipt]', function () {        
        var pos = $(this).offset();
        topPos = pos.top;
        leftPos = pos.left + 55;        
        line = $("#line").val();
        dts = $("#dt_s").val();
        dte = $("#dt_e").val();
        clientID = $(this).prev().val();
        if ($('#receipt_detail').hasClass('hide'))
            $('#receipt_detail').removeClass('hide');
        $('.receipt').css('top', topPos);
        $('.receipt').css('left', leftPos);    
        param = "lines=" + line +"&dt_s="+dts+"&dt_e="+dte+"&clientID="+clientID;
        ajaxQuery($(this).attr('href'),param,recipeDetail,true);         
        return false        
    })

    $('#receipt_detail').on('click', '.close', function () {
        $('#receipt_detail > *').remove();
        if (!$('#receipt_detail').hasClass('hide'))
            $('#receipt_detail').addClass('hide');
    });

    $('#receipt_detail').on('click', 'button', function () {
        $('#receipt_detail').load(
                $(this).attr('href') + ' #receiptDetail > *', 
                {serializeData: $('#receipt_detail > div input').serialize(), edit: ''},
                function (response, status, xhr) {
                    if (status == "error") {                        
                        throw_message(+msg + xhr.status + " " + xhr.statusText);
                    }
                }
        );
        return false
    });

    $('#export').click(function () {
        $('#error').remove();
    });

    $('#receipt_detail').on('click', '#viewRequest', function () {        
        if ($('#recipeClaim').hasClass('clicked')) {
            $(this).text('показать заявки');
            $('#recipeClaim').removeClass('clicked');
            $('#recipeClaim > *').remove();
        } else {
            $('#recipeClaim').addClass('clicked');
            $(this).text('скрыть заявки');
            line = $("#hiddenLine").val(); 
            dts = $("#dt_s").val();
            dte = $("#dt_e").val();
            clientID = $("#hiddenClID").val();
            param = "lines=" + line + "&dt_s=" + dts + "&dt_e=" + dte + "&clientID=" + clientID;
            ajaxQuery(
                $(this).attr('href'), 
                param,                
                recipeClaim,
                true
                );                        
        }
        $('#rolUpDownRequest').toggleClass('roll_up roll_down');
        return false
    });
    
    $(".admin_menu").on('click','a', function(){
        $(".admin_menu").removeClass('admin_active_menu');
        url = $(this).attr('href');  
        $.ajaxSetup({global: false});
        $('.right').load(url + ' .right > *');
        $(this).parent().toggleClass('admin_active_menu');
        history.pushState(null, '', url); 
        return false;
    });    
    
    $('.right').on('click', '#plc_add', function(){        
        ajaxQuery(
                $(this).attr('formaction'), 
                $(this).parent().parent().serialize(),                
                plcAdd
                );         
        return false;
    });
    
    $('.right').on('click', '#plc_io_add', function(){        
        ajaxQuery(
                $(this).attr('formaction'), 
                $(this).parent().parent().serialize(),                
                plcAdd
                );         
        return false;
    });
    
    $('.right').on('click', '.view_plc', function(){
        $('.plc').css('background-color','#fff')
        if ($('#plc_io').hasClass('hide')){
            $('#plc_io').removeClass('hide');
        }
        $(this).children('.plc').css('background-color','#71bb71');        
        $('#plc_io').children('form').children('#plc_id').val(
                $(this).children('.plc').children('input:hidden').val()
                )
    });    
    
    $(".steam").on("change", "input[type='checkbox']", function(){       
        $(this).prop('disabled', true);
        plc = {
            steam:  $(this).prop("checked"),
            name:   $(this).attr('name')
        };                
        ajaxQuery(location.href+'/switchSteam', plc, steamSwitchCallback);        
        state = $(this).prop("checked") == true ? false : true;
        $(this).prop("checked", state);                 
    });
    
    $('#receipt_detail').on('click','#print', function(){          
        receiptDetail = $('.receipt_header');          
        printElem($("."+$(this).attr('data-toggle')).html(), receiptDetail.html());
    })
});

function viewPage(data){    
    $(".right").html(data);
}

function plcAdd(data){    
    throw_message(data.message);
}

function recipeClaim(data){
    $('#recipeClaim').html(data);
}

function recipeDetail(data){    
    $('#receipt_detail').html(data);
}

function concreteRecipeDetail(data){    
    line = "<div class='bsu_components'><div><p>Наименование</p><p>Количество</p></div>";
    $.each(data, function(key, val){        
        line += "<div><p>" + val.name + "</p><p>" + val.val + "</h3></div>";        
    });  
    line += "</div>";    
    $(line).replaceAll('.bsu_components');
}

function laravelValidResponse(JSONResponse){   
    if(typeof(JSONResponse) == "undefined"){
        return;
    }
    
    if('error' in JSONResponse){
        $.each(JSONResponse.errors,function(key, value){
            $('input[name='+key+']').attr('placeholder',value).parent().addClass('valid-error');
        });    
    }    
    
    if('message' in JSONResponse){        
        throw_message(JSONResponse.message);        
    }    
}

function ajaxQuery(url, postParam, callback, global = false, type = 'POST', async = true){           
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        global: global,
        async: async,
        type: type,
        url: url,
        data: postParam,
        fail: function(jqXHR, statusText){            
            if(jqXHR.status = 422){
                laravelValidResponse(jqXHR.responseJSON);
            }
        },
        success: function(data){
            callback(data);
        }
    });
    
    return false;
}

function steamingResponse(data){         
    status = 'status' in data ? data.status : 'ok';
    if(status == 'error'){
        throw_message(data.data);
        return;
    }
    $.each(data, function(plcName, plc){         
        if(plc.status == 'error'){       
            if($("#"+plcName+" > #error").hasClass('hide')){
                $("#"+plcName+" > #error").removeClass('hide');
                $("#"+plcName+" > #error > p").text(plc.error);
            }            
            return;
        }
        
        if(!$("#"+plcName+" > #error").hasClass('hide')){
            $("#"+plcName+" > #error").addClass('hide');
        }
        
        if(plc.msg[3] > 0){              
            steamChartData.setValue(0, 1, plc.msg[0]);     
            steamChartData.setValue(1, 1, plc.msg[1]);              
            steamChartData.setValue(2, 1, plc.msg[4]*10);             
            steamChart[plcName].draw(steamChartData, steamChartOptions);            
        } 
        
        $("#"+plcName+"_steam").prop("checked", plc.msg[3]);
    });    
}

function steamSwitchCallback(data){    
    if('error' in data){
        throw_message(data.error);
        return;
    }
    
    $.each(data, function(plcName, value){    
        $("#"+plcName+"_steam").prop('disabled', false);
        throw_message(plcName +":"+ value.msg);        
    });         
}

function steaming() {
    steamChartData = google.visualization.arrayToDataTable([
        ["°C", 0],
        ['°C', 0],
        ['Клапан', 0],
    ], true);

    steamChartOptions = {
        min: 0,
        max: 100,
        width: 400, height: 120,
        redFrom: 90, redTo: 100,
        yellowFrom: 75, yellowTo: 90,
        minorTicks: 5
    };

    $('.steam').each(function (index, value) {
        devName = $(this).attr('id');
        steamChart[devName] = new google.visualization.Gauge($(this).children("#" + devName + "_chart")[0]);
        steamChart[devName].draw(steamChartData, steamChartOptions);                
    });  

    steamTimer = setInterval(function () {
        ajaxQuery(location.href+'/getSteamData', '', steamingResponse);
    }, 200);
    
}

function printElem(data, titleData = '')
{    
    var mywindow = window.open('', 'Отчеты БСЦ', 'height=400,width=600');
    mywindow.document.write('<html><head><title>Отчеты БСЦ</title>');
    mywindow.document.write('<link rel="stylesheet" href="../css/style.css" />');
    mywindow.document.write('</head><body ><div id="section-to-print">');
    mywindow.document.write('<div class="printElem">'+titleData+'</div>');  
    mywindow.document.write(data);  
    mywindow.document.getElementById('print').remove();
    mywindow.document.getElementById('close').remove();
    mywindow.document.write('</div></body></html>');    
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10   
    setTimeout(function() {
        mywindow.print();        
        mywindow.close();
    }, 100); 
    
    return true;
}

function throw_message(str) 
{
    elem = document.createElement('p')    
    elem.id = 'notice_msg'   
    elem.innerHTML = str  
    
    $("#notice_box").append(elem);
    $(elem).fadeIn(500)
            .delay(3000)
            .fadeOut(500)
            .delay(100)
                .queue(function() {
                    $(this).remove();
                });
}