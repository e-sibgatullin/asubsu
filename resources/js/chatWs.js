let socketChat = new WebSocket("ws://asubsu.local:6001/ws-chat")      

document.getElementsByClassName('open-button').onclick = openForm;
document.getElementsByClassName('btn-close').onclick = closeForm;

socketChat.onmessage = function(event){
    console.log(event)
}

socketChat.onerror = function(error){
    console.log(error)
}

socketChat.onclose = function(event){
    console.log(event)
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}