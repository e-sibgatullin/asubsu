<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $table = 'batches'; 
    
    public function batchCompositions() {
        return $this->hasMany('App\Models\BatchComposition', 'batchID','id');
    }
    
    public function components() {
        return $this->belongsToMany('App\Models\Component','batchescompositions','batchID','componentID')->withPivot('m');
    }
}
