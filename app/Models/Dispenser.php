<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dispenser extends Model
{
    public function hoppers() 
    {
        return $this->belongsToMany('App\Models\Hopper','hoppersLinkDispensers','dispenserID','hopperID')->where('active', 1);
    }
}
