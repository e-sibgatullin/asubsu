<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchComposition extends Model
{
    protected $table = 'batchescompositions';    
}
