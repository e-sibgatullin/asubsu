<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plc extends Model
{
    protected $guarded = ['id'];
    protected $table = 'plc';
    
    public $timestamps = false;    
}
