<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Recipe extends Model
{
    protected $table = 'receipts';    
    public $timestamps = false;
    
    public function getRecipeConsolidate(array $param) 
    {
        if(!is_array($param)){
            return;
        }        
        
        $sql = "SELECT 
                rec.clientID as clientID,    
                rec.receiptID AS receipt,
                r.Number AS receiptNumber,
                r.brand AS receiptBrand,
                r.shortName AS receiptName,
                r.group AS receiptGroup,
                cl.shortName AS clientName,
                IF((rec.batchM = 0), 0, rec.rq_volume) AS rq_volume,
                rec.rq_volume * (rec.batchM / (SUM(comp.M) * rec.rq_volume)) AS tt_volume,
                rec.M400_PV / 1000 AS M400_PV,
                SUM(IF((comp.componentID = 1),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS M400_SP,
                rec.M500_PV / 1000 AS M500_PV,
                SUM(IF((comp.componentID = 2),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS M500_SP,
                rec.M550_PV AS M550_PV,
                SUM(IF((comp.componentID = :M550),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume),
                    0)) AS M550_SP,
                rec.M600_PV / 1000 AS M600_PV,
                SUM(IF((comp.componentID = 4),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS M600_SP,
                rec.W_PV / 1000 AS W_PV,
                SUM(IF((comp.componentID = 5),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS W_SP,
                rec.A1_PV AS A1_PV,
                SUM(IF((comp.componentID = 6),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume),
                    0)) AS A1_SP,
                rec.A2_PV AS A2_PV,
                SUM(IF((comp.componentID = 7),
                    comp.m * IF((rec.rq_volume = 0),
                        rec.emptyReqVolume,
                        rec.rq_volume),
                    0)) AS A2_SP,
                rec.A3_PV AS A3_PV,
                SUM(IF((comp.componentID = 8),
                    comp.m * IF((rec.rq_volume = 0),
                        rec.emptyReqVolume,
                        rec.rq_volume),
                    0)) AS A3_SP,
                rec.A4_PV AS A4_PV,
                SUM(IF((comp.componentID = 9),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume),
                    0)) AS A4_SP,
                rec.A5_PV / 1000 AS A5_PV,
                SUM(IF((comp.componentID = 10),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS A5_SP,
                rec.CS1_PV / 1000 AS CS1_PV,
                SUM(IF((comp.componentID = 11),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS1_SP,
                rec.CS2_PV / 1000 AS CS2_PV,
                SUM(IF((comp.componentID = 12),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS2_SP,
                rec.CS3_PV / 1000 AS CS3_PV,
                SUM(IF((comp.componentID = 13),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS3_SP,
                rec.CS4_PV / 1000 AS CS4_PV,
                SUM(IF((comp.componentID = 14),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS4_SP,
                rec.CS5_PV / 1000 AS CS5_PV,
                SUM(IF((comp.componentID = 15),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS5_SP,
                rec.CS6_PV / 1000 AS CS6_PV,
                SUM(IF((comp.componentID = 16),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CS6_SP,
                rec.CLAYITE_PV / 1000 AS CLAYITE_PV,
                SUM(IF((comp.componentID = 17),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS CLAYITE_SP,
                rec.MS_PV / 1000 AS MS_PV,
                SUM(IF((comp.componentID = 18),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS MS_SP,
                rec.TS_PV / 1000 AS TS_PV,
                SUM(IF((comp.componentID = 19),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS TS_SP,
                rec.RS_PV / 1000 AS RS_PV,
                SUM(IF((comp.componentID = 20),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume) / 1000,
                    0)) AS RS_SP,
                rec.SIKA5600_PV AS SIKA5600_PV,
                SUM(IF((comp.componentID = :SIKA),
                    comp.m * IF((rec.batchM = 0), 0, rec.rq_volume),0)) AS SIKA5600_SP
            FROM
                (SELECT 
                    req.request AS request,
                        req.receiptID AS receiptID,
                        req.clientID AS clientID,
                        SUM(IF((req.batchM = 0), 0, req.rq_volume)) AS rq_volume,
                        SUM(req.rq_volume) AS emptyReqVolume,
                        SUM(req.batchM) AS batchM,
                        SUM(M400_PV) AS M400_PV,
                        SUM(M500_PV) AS M500_PV,
                        SUM(M550_PV) AS M550_PV,
                        SUM(M600_PV) AS M600_PV,
                        SUM(W_PV) AS W_PV,
                        SUM(A1_PV) AS A1_PV,
                        SUM(A2_PV) AS A2_PV,
                        SUM(A3_PV) AS A3_PV,
                        SUM(A4_PV) AS A4_PV,
                        SUM(A5_PV) AS A5_PV,
                        SUM(CS1_PV) AS CS1_PV,
                        SUM(CS2_PV) AS CS2_PV,
                        SUM(CS3_PV) AS CS3_PV,
                        SUM(CS4_PV) AS CS4_PV,
                        SUM(CS5_PV) AS CS5_PV,
                        SUM(CS6_PV) AS CS6_PV,
                        SUM(CLAYITE_PV) AS CLAYITE_PV,
                        SUM(MS_PV) AS MS_PV,
                        SUM(TS_PV) AS TS_PV,
                        SUM(RS_PV) AS RS_PV,
                        SUM(SIKA5600_PV) AS SIKA5600_PV
                FROM
                    (SELECT 
                    req.id AS request,
                        req.receiptID AS receiptID,
                        req.clientID AS clientID,
                        req.volume AS rq_volume,
                        SUM(IF(ISNULL(bc.m), 0, bc.m)) AS batchM,
                        SUM(IF((bc.componentID = 1), (bc.m), 0)) AS M400_PV,
                        SUM(IF((bc.componentID = 2), (bc.m), 0)) AS M500_PV,
                        SUM(IF((bc.componentID = :M550), (bc.m), 0)) AS M550_PV,
                        SUM(IF((bc.componentID = 4), (bc.m), 0)) AS M600_PV,
                        SUM(IF((bc.componentID = 5), (bc.m), 0)) AS W_PV,
                        SUM(IF((bc.componentID = 6), (bc.m), 0)) AS A1_PV,
                        SUM(IF((bc.componentID = 7), (bc.m), 0)) AS A2_PV,
                        SUM(IF((bc.componentID = 8), (bc.m), 0)) AS A3_PV,
                        SUM(IF((bc.componentID = 9), (bc.m), 0)) AS A4_PV,
                        SUM(IF((bc.componentID = 10), (bc.m), 0)) AS A5_PV,
                        SUM(IF((bc.componentID = 11), (bc.m), 0)) AS CS1_PV,
                        SUM(IF((bc.componentID = 12), (bc.m), 0)) AS CS2_PV,
                        SUM(IF((bc.componentID = 13), (bc.m), 0)) AS CS3_PV,
                        SUM(IF((bc.componentID = 14), (bc.m), 0)) AS CS4_PV,
                        SUM(IF((bc.componentID = 15), (bc.m), 0)) AS CS5_PV,
                        SUM(IF((bc.componentID = 16), (bc.m), 0)) AS CS6_PV,
                        SUM(IF((bc.componentID = 17), (bc.m), 0)) AS CLAYITE_PV,
                        SUM(IF((bc.componentID = 18), (bc.m), 0)) AS MS_PV,
                        SUM(IF((bc.componentID = 19), (bc.m), 0)) AS TS_PV,
                        SUM(IF((bc.componentID = 20), (bc.m), 0)) AS RS_PV,
                        SUM(IF((bc.componentID = :SIKA), (bc.m), 0)) AS SIKA5600_PV
                FROM
                    requests AS req
                LEFT JOIN batches AS b ON b.requestID = req.id
                LEFT JOIN batchescompositions AS bc ON bc.batchID = b.id
                WHERE
                    req.started BETWEEN STR_TO_DATE(:dt_s, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:dt_e, '%Y-%m-%d %H:%i:%s')
                GROUP BY req.id) AS req
                GROUP BY req.receiptID , req.clientID) AS rec
                    JOIN
                compositions AS comp ON comp.receiptID = rec.receiptID
                    JOIN
                receipts AS r ON r.id = rec.receiptID
                    JOIN
                clients AS cl ON cl.id = rec.clientID
            GROUP BY rec.receiptID , rec.clientID
            ORDER BY rec.clientID , receiptGroup , receiptNumber , receiptBrand , rec.receiptID";
        
        $sql = str_replace(':M550', $param['M550'], $sql); 
        $sql = str_replace(':SIKA', $param['SIKA'], $sql);        
        $recipeArray = DB::connection($param['lines'])
                ->select($sql,[                      
                    'dt_s'=>date('Y-m-d H:i:s', strtotime($param['dt_s'])),
                    'dt_e'=>date('Y-m-d H:i:s', strtotime($param['dt_e'])),
                        ]);
                        
        return $recipeArray;
    }
    
    public function components() 
    {
        return $this->belongsToMany('App\Models\Component','compositions','receiptID','componentID')->withPivot('m');
    }
    
    public function requests() {
        return $this->hasMany('App\Models\Claim', 'receiptID','id');
    }
}
