<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hopper extends Model
{
    public function component() 
    {
        return $this->belongsToMany('App\Models\Component','componentsLinkHoppers','hopperID','compID');
    }
    
    public function dispenser() 
    {
        return $this->belongsToMany('App\Models\Dispenser','hoppersLinkDispensers','hopperID','dispenserID')->where('active', 1);
    }
}
