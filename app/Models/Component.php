<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    public function receipts() 
    {
        return $this->belongsToMany('App\Models\Recipe','compositions','componentID','receiptID')->withPivot('m');
    }
    
    public function hoppers() 
    {
        return $this->belongsToMany('App\Models\Hopper','componentsLinkHoppers','compID','hopperID')->where('active', 1);
    }
}
