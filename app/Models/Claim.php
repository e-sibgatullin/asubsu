<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Claim extends Model
{
    protected $table = 'requests';

    public function batches() {
        return $this->hasMany('App\Models\Batch','requestID','id');
    }
    
    public function recipe() {
        return $this->belongsTo('App\Models\Recipe','receiptID','id');
    }
    
    public function client() {
        return $this->belongsTo('App\Models\Client','clientID','id');
    }
    
    public function getClaimForRecipe($param) {
        $sql = "SELECT
                reqFact.request,
                reqFact.receiptID,
                reqFact.clientID,
                reqFact.orData,
                reqFact.orQuantity,
                round(reqFact.orQuantity * (reqFact.batchM / SUM(receiptM.m * reqFact.orQuantity)),2) AS factQuantity
                FROM
                (SELECT
                req.id AS request,
                req.receiptID AS receiptID,
                req.clientID AS clientID,
                req.volume AS orQuantity,
                req.accepted AS orData,
                b.id AS batch,
                SUM(bc.m) AS batchM
                FROM
                requests AS req
                LEFT JOIN batches AS b ON b.requestID = req.id
                JOIN batchescompositions AS bc ON bc.batchID = b.id
                WHERE
                req.started BETWEEN STR_TO_DATE(:dt_s, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(:dt_e, '%Y-%m-%d %H:%i:%s')
                AND req.receiptID = :receiptID
                AND req.clientID = :clientID
                GROUP BY req.id) AS reqFact
                JOIN
                compositions AS receiptM ON receiptM.receiptID = reqFact.receiptID
                GROUP BY reqFact.request";
        
        $ClaimArray = DB::connection($param['lines'])
                ->select($sql,[                      
                    'dt_s'=>date('Y-m-d H:i:s', strtotime($param['dt_s'])),
                    'dt_e'=>date('Y-m-d H:i:s', strtotime($param['dt_e'])),
                    'receiptID'=>$param['recipeID'],
                    'clientID'=>$param['clientID']
                        ]);
                        
        return collect($ClaimArray);
    }
}
