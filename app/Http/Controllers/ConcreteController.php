<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Custom\Concrete\Concrete;
use Illuminate\Support\Facades\Cache;
use App\Models\Recipe;
use App\Models\Hopper;
use App\Models\Dispenser;
use App\Models\Mixer;
use App\Models\OptionalDev;
use Illuminate\Support\Facades\Crypt;

class ConcreteController extends Controller
{       
    public function bsu(Request $request) 
    { 
        //dd(Crypt::decrypt('eyJpdiI6InJRYk0ya2l3VGQ3Qkw5dFJWVXh3NXc9PSIsInZhbHVlIjoiRVh0U0JYUlRZQlg5S3NDMG5tQ1pjbjZSeENIaGxwZ2JoMzFjTkZnU2s0aWV4cTV4NDJ1XC9XSnlLVVpqXC9kZEE4IiwibWFjIjoiYmFiODQwMTBlMWQzMjM3OWM1NDQ5NjA5MjBkN2FiYjI5OGVjYmUzYjRlNDY4MmNlOTRjOTk2YjYyMjM1ZTZkZiJ9', false));
        $recipes = Recipe::all();        
        $devices['hopper'] = Hopper::where('active',1)->get();             
        $devices['other'] = OptionalDev::where('active',1)->get();        
        $devices['dispencer'] = Dispenser::where('active',1)->get();             
        $devices['mixer'] = Mixer::where('active',1)->get();                             
        
        return view('pages.concrete',['title'=>':: БСУ', 'recipes' => $recipes, 'devices' => $devices]);
    }       
}
