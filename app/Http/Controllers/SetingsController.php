<?php

namespace App\Http\Controllers;

use App\Models\Plc;
use App\Models\Plc_io;
use App\Models\Device;
use App\Models\Component;
use Illuminate\Http\Request;

class SetingsController extends Controller
{
    public function index() 
    {
        return view('setings.main', ['title'=>' :: Настройки БСУ']);
    }
    
    public function device() 
    {
        return view('setings.device', ['title'=>' :: Настройки БСУ :: Оборудование']);
    }
    
    public function plc() 
    {
        $plc = plc::all();                
        return view('setings.plc', ['title'=>' :: Настройки БСУ :: Контролер','plc'=>$plc]);
    }
    
    public function component() 
    {        
        return view('setings.components', ['title'=>' :: Настройки БСУ :: Компоненты']);
    }
    
    public function recipe() 
    {
        return view('setings.recipe', ['title'=>' :: Настройки БСУ :: Рецепты']);
    }
    
    public function plcAdd(Request $request) 
    {
        $this->validate($request, [
            'name'=>'string|min:3',
            'ip'=>'required|ip',
            'line'=>'required'
        ]);        
        $newPLC = Plc::Create([
            'name'=> request()->name,
            'ip'=> request()->ip,
            'line'=> request()->line
        ]);
        if($request->ajax()){
            return response()->json(['message'=>'Запись успешно добавлена.']);
        }
        
        return back();
    }
    
    public function plcIOAdd(Request $request) 
    {
        $this->validate($request, [
            'plc_id'=>'required|numeric',
            'io_numb'=>'required|numeric',
            'io_data_type'=>'required|numeric',
            'io_fc'=>'required|numeric'
        ]);        
        $newIO = Plc_io::Create([
            'id_plc'=> request()->plc_id,
            'io_numb'=> request()->io_numb,
            'io_type_data'=> request()->io_data_type,
            'io_fc'=> request()->io_fc
        ]);        
        if($request->ajax()){
            return response()->json(['message'=>'Запись успешно добавлена.']);
        }
        
        return back();
    }
}
