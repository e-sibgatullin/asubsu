<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\v1\Recipe as RecipeRequest;
use App\Models\Recipe;

class RecipeController extends Controller
{
    public function getRecipe(Request $request, $id = null) 
    {        
        $recipe = $id ? Recipe::find($id) : Recipe::all();
        $statusCode = empty($recipe) ? '404' : '200';        
        
        return response()->json($recipe, $statusCode);
    }  
    
    public function addRecipe(RecipeRequest $request) 
    {     
        $recipe = new Recipe();
        
        $recipe->shortName = $request->shortName;
        $recipe->group = $request->group;
        $recipe->number = $request->number;
        $recipe->brand = $request->brand;
        $state = $recipe->save();
        
        $insert = $state ? $recipe->id : 'false' ;
        $stateCode = $state ? '201' : '501';
        return response()->json(['insertId' => $insert], $stateCode);
    }
    
    public function delRecipe(RecipeRequest $request, $id) 
    {
        $recipe = Recipe::find($id);
        if(empty($recipe)){
            return response()->json(['staus' => 'error', 'msg'=>"entry $id not found"], '400');
        }
        
        $recipe->delete();        
        return response()->json(['staus' => 'ok', 'msg'=> "deleted $id"], '200');
    }
}
