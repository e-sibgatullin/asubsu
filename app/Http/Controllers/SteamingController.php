<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SteamingController extends Controller
{
    public function index() 
    {        
        return view('pages.steaming',['title'=>':: Пропарка', 'facilitys'=> $this->getSteamData()]);
    }
    
    public function getSteamData() 
    {        
        $steamConnect = resolve('App\Custom\ConnectSteaming');        
        $data = $steamConnect->send('all')->getResponse();                
        $steamConnect->close();  
        if(request()->ajax()){
            return response()->json($data);
        }
        
        return $data;        
    }
    
    public function switchSteam(Request $request) 
    {
        $swich = $request->input('steam') == 'true' ? 'on' : 'off';
        $plcName = $request->input('name');
        $steamConnect = resolve('App\Custom\ConnectSteaming');        
        $response = $steamConnect->send("$plcName=$swich")->getResponse();                
        $steamConnect->close(); 
        return response()->json($response);
    }
    
    public function lineChartData(Request $request) 
    {
        
    }
}
