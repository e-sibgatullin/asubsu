<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Custom\SocialAuth\custumSocialAuthFacade as Socialite;

class AuthController extends Controller
{
    public function getLogin() 
    {         
        return view('forms.logIn', ['title' => ':: вход на портал']);
    }
    
    public function postLogin(Request $request) 
    {     
        $this->validate($request, [
            'login'=>'required|min:3',
            'password'=>'required|min:5',
        ]);
        
        if (Auth::attempt($request->only('login','password'))) {                          
            return redirect()->intended('/');
        }        
        return redirect()->intended(route('login'));
    }
    
    public function getLogout() 
    {
        Auth::logout();
        return redirect('/');
    }
    
    public function getRegister(Request $request) 
    {        
        return view('forms.register',['title'=>':: регистрация пользователей']);        
    }
    
    public function postRegister($socialName = "") 
    {        
        return view('forms.register',['title'=>':: регистрация пользователей']);        
    }
    
    public function socialAuth(Request $request, $socialName = "vkontakte") 
    {        
        return Socialite::driver($socialName)->redirect();          
    } 
    
    public function socialAuthCallback(Request $request) 
    {
        $user = Socialite::driver('vkontakte')->user();          
        
        $userbd = User::firstOrCreate(
                ['login' => $user->email],
                ['login' => $user->email,
                    'password' => $user->token,
                    'socialID' => $user->id,
                    'socialToken' => $user->token,
                    //'expires_in' => $user->expiresIn,
        ]);          
        
        Auth::login($userbd);
        return redirect('/');
    }
}
