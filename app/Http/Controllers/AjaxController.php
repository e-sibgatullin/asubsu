<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;

class AjaxController extends Controller
{
    public function getRecipeDetail(Request $request, $recipeID) 
    {
        $result = [];
        $recipe = Recipe::on($request->input('lines'))
                ->find($recipeID);
        
        foreach($recipe->components as $comp){
            $result[] = ['name' => $comp->shortName, 'val' => $comp->pivot->m];            
        }
        return response()->json($result);
    }
}
