<?php

namespace App\Http\Controllers;
use App\Http\Requests\Report;
use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Models\Claim;

class ReportController extends Controller
{
    public function recipe(Report $request) 
    {        
        $recipeM = new Recipe();        
        $data = $request->all();        
        $recipeArray = $recipeM->getRecipeConsolidate($data);  
                
        $recipeArray = resolve('Counting')->countTotalGroup($recipeArray, [
            'clientName',
            'receiptGroup',
            'receiptBrand'
            ]);        
        
        $data += [
            'title'=>':: Отчет по рецептам БСЦ',
            'arrayData' => $recipeArray
            ];        
        return view('report.recipe', $data); 
    }
    
    public function recipeXML(Report $request) 
    {        
        $recipeM = new Recipe();                
        $recipeArray = $recipeM->getRecipeConsolidate($request->all());        
        $XML = resolve('ReportXML');
        $XML->Create($recipeArray, [
            'line'=>$request->input('lines'),
            'date_creat'=> date('Y-m-d'),
            ]);         
        return response()
                ->download($XML->save())
                ->deleteFileAfterSend(true);
    }
    
    public function recipeDetail(Report $request, $idRecipe) 
    {
        $recipe = Recipe::on($request->input('lines'))
                ->find($idRecipe);        
        return view('report.recipeDetail',[
            'recipe'=>$recipe,
            'data'=>$request->all()
                ]);
    }
    
    public function recipeViewRwquest(Report $request, $receiptID) 
    {
        $claimM = new Claim();
        $param = $request->all();
        $param['recipeID'] = $receiptID;        
        $recRequest = $claimM->getClaimForRecipe($param);        
        return view('report.recipeClaim',['requests'=>$recRequest]);
    }
    
    public function claim(Report $request) {
        $claims = Claim::on($request->input('lines'))->where([['started', '>=',$request->input('dt_s')],['started','<=',$request->input('dt_e')]])->get();                
        return view('report.claim',[
            'title'=>':: Отчет по заявкам БСЦ',
            'claims'=>$claims,
            'lines'=>$request->input('lines'),
            'dt_s'=>$request->input('dt_s'),
            'dt_e'=>$request->input('dt_e')
        ]);
    }
}
