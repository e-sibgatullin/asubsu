<?php

namespace App\Http\Middleware;

use Closure;

class FillAdditionalFields
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {                
        if(!$request->has('dt_s')){
            $request->request->add(['dt_s'=>date('Y-m-d').'T00:00']);
        }
        
        if(!$request->has('dt_e')){
            $request->request->add(['dt_e'=>date('Y-m-d').'T23:59']);
        }
        
        if(!$request->has('lines')){
            $request->request->add(['lines'=>'lineOne']);
        }
        
        $M550 = ($request->input('lines') == env('DB_CONNECTION')) ? 23 : 3;
        $SIKA = ($request->input('lines') == env('DB_CONNECTION')) ? 24 : 22;
        $request->request->add(['M550'=>$M550]);
        $request->request->add(['SIKA'=>$SIKA]);
        return $next($request);
    }
}
