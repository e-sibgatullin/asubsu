<?php

namespace App\Http\Requests\API\v1;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class recipe extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->hasHeader('authorization')){
            return false;
        }
        
        $token = array_shift($this->header()['authorization']);
        $user = User::where('apiToken', $token)->first();
        return empty($user) ? false : true;        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {       
        if($this->method('delete')){
            return [];
        }
        
        return [
            'shortName' => 'required|string|max:45',
            'group' => 'required|string|max:45',
            'number' => 'required|string|max:15',
            'brand' => 'required|string|max:45'
        ];
    }
}
