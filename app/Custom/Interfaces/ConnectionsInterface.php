<?php
namespace App\Custom\Interfaces;

interface ConnectionsInterface
{
    public function getConnect($host, $port);    
}
