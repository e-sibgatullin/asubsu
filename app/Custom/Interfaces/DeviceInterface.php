<?php

namespace App\Custom\Interfaces;

use \Modbus\ModbusTCP;
use Illuminate\Database\Eloquent\Model;

interface DeviceInterface {    
    public function __construct(ModbusTCP $modbus, Model $model); 
    public function open();
    public function close();    
} 
