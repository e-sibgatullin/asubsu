<?php
namespace App\Custom\Interfaces;
use App\Custom\Concrete\Concrete;

interface ConcreteStageInterface
{
    public function __construct(Concrete $concrete); 
    public function run();     
}
