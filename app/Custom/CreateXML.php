<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom;

/**
 * Description of CreateXML
 *
 * @author Evgeniy
 */
class CreateXML 
{
    private $pathToFile; 
    private $domXML;
    private $rootNode;
    private $rootNodeAttr;
    private $nodeDp;
    private $NodeReceipt;


    public function __construct($pathToSave) 
    {
        $this->pathToFile = $pathToSave;        
        $this->domXML = new \domDocument("1.0", "utf-8"); 
    }    
    
    public function create(array $arrayData, array $rootAttr) 
    {        
        if(empty($arrayData)){
            return false;
        }
        $this->rootNodeAttr = $rootAttr;
        $this->createNode($this->rootNode,'bsc-doz');              
        $this->setAtribute($this->rootNodeAttr, $this->rootNode);                
        $this->domXML->appendChild($this->rootNode);        
        foreach ($arrayData as $key => $value) {
            if (!is_object($this->nodeDp) || $this->nodeDp->getAttribute('id') <> $value->clientName) {
                $this->createNode($this->nodeDp,'dep');
                $this->setAtribute(['id'=>$value->clientName], $this->nodeDp);                
                $this->attachNode($this->rootNode, $this->nodeDp);
            } 
            
            $this->createNode($this->NodeReceipt,'receipt');
            $this->setAtribute(['id'=>$value->receiptNumber], $this->NodeReceipt);
            $this->attachNode($this->nodeDp, $this->NodeReceipt);
            $this->addDetailToNodeReceipt($value);                
            $this->attachNode($this->nodeDp, $this->NodeReceipt);
        }         
    }
    
    private function createNode(&$peremNode, $nameNode) 
    {
        $peremNode = $this->domXML->createElement($nameNode); 
    }   
    
    private function setAtribute(array $attr, $node) 
    {
        foreach ($attr as $key => $value) {
            $node->setAttribute($key, $value);
        }
    }
    
    private function addDetailToNodeReceipt($nodeData) 
    {
        foreach ($nodeData as $key => $value) {
            $detail = $this->domXML->createElement($key, round($value, 2)); 
            $this->attachNode($this->NodeReceipt, $detail);
        }         
    }
    
    private function attachNode($node, $attached) 
    {
        $node->appendChild($attached);
    }
    
    public function save() 
    {        
        $filename = date('Y-m-d') .'-'. $this->rootNode->nodeName .'-'. $this->rootNode->getAttribute('line') .'.xml';        
        $this->domXML->save($this->pathToFile . DIRECTORY_SEPARATOR . $filename); 
        return $this->pathToFile . DIRECTORY_SEPARATOR . $filename;
    }
}
