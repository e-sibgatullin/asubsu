<?php
namespace App\Custom;

use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;
use Modbus\Exceptions\ModbusExceptions;
use Modbus\Exceptions\ConnectExceptions;
use Modbus\Exceptions\ParserExceptions;
use App\Exceptions\ConcreteException;
use App\Custom\UserForWs;

class WSConcrete implements MessageComponentInterface
{      
    private $clients = [];             
 
    public function onOpen(ConnectionInterface $connection)
    {        
        $connection->socketId = sprintf('%d.%d', random_int(1, 1000000000), random_int(1, 1000000000));                                  
        $userWs = new UserForWs($connection);
        $this->clients[$connection->socketId] = $userWs;
        if(!$userWs->isAuth()){
            $this->closeConnect('Необходимо авторизоваться.', $connection);                        
            return;
        }
        
        if(count($this->clients) > 1) {
            $this->closeConnect('Больше одного подключения не допустимо.', $connection);                        
            return;
        }    
        
        $connection->app = resolve('ConcreteLine1'); 
        $connection->app->setWsConn($connection);        
    }
    
    public function onClose(ConnectionInterface $connection)
    {
        unset($this->clients[$connection->socketId]);
    }

    public function onError(ConnectionInterface $connection, \Exception $e)
    {
        //echo get_class($connection->app).PHP_EOL;
        //echo 'Error: '.$e->getTraceAsString();
    }

    public function onMessage(ConnectionInterface $connection, MessageInterface $msg)
    {        
        $msgJson = json_decode($msg->getPayload());                  
        
        try {
            $connection->app->{$msgJson->method}(...$msgJson->properties);            
        } catch (ModbusExceptions | ConnectExceptions | ParserExceptions | ConcreteException $e) {                        
            $this->sendError($e->getMessage(), $connection);
        }        
    }    
    
    private function sendError($msg, $connection)
    {
        $data = [
                'state' => 'error',
                'data' => $msg
            ];
        
        $connection->send(json_encode($data));
    }    
    
    private function closeConnect($msg, $connection)
    {
        $this->sendError($msg, $connection);            
        $connection->close();
    }    
}