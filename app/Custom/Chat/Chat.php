<?php
namespace App\Custom\Chat;

use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use App\Custom\UserForWs;

class Chat 
{    
    CONST STAGE_CONN = 0;
    CONST STAGE_LEFT_CHAT = 1;
    CONST STAGE_DIALOG = 2;
    CONST STAGE_ERROR = 3;
    
    public $id;
    public $appKey;
    public $secret;
    
    private $client;        

    public function __construct($id, string $appKey, string $secret) 
    {
        $this->id = $id;
        $this->appKey = $appKey;
        $this->secret = $secret;
    }    
    
    public function newConn(ConnectionInterface $connection) 
    {
        $this->client[$connection->socketId] = $connection;           
        $this->response($connection, self::STAGE_CONN);
    }
    
    public function leftChat(ConnectionInterface $connection)
    {
        unset($this->client[$connection->socketId]);
        $this->response($connection, self::STAGE_LEFT_CHAT);
    }
    
    public function dialog(ConnectionInterface $connection, MessageInterface $msg)
    {
        $payload = json_decode($msg->getPayload());
        $this->response($connection, self::STAGE_DIALOG, $payload->msg , $payload->dst);
    }
    
    private function response(ConnectionInterface $conn, int $stage, $msg = null, string $dst = null) 
    {
        switch ($stage) {
            case self::STAGE_CONN:
                $dst = 'all';
                $msg = array_keys($this->client);
                break;
            case self::STAGE_LEFT_CHAT:
                $dst = 'all';
                $msg = $conn->socketId;
                break;            
            case self::STAGE_ERROR:
                $dst = $conn->socketId;                
                break;
        }
        
        $response = [
        'type' => $stage,
        'src' => $conn->socketId,
        'dst' => $dst,
        'msg' => $msg
        ];    
                
        if(array_key_exists($dst, $this->client)){
            $this->client[$dst]->send(json_encode($response));
        }        
        
        if($dst === 'all'){
            foreach ($this->client as $socketId => $connect) {
                $connect->send(json_encode($response));
            }
        }
    }
}
