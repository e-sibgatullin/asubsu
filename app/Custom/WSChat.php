<?php
namespace App\Custom;

use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;
use App\Custom\Chat\Chat;

class WSChat implements MessageComponentInterface
{
    private $app; 
    private $onePartConnId = 0;
    private $twoPartConnId = 0;
    
    public function __construct()
    {
        $this->app = new Chat(1,'1','1');
    }
        
    public function onOpen(ConnectionInterface $connection)
    {        
        $connection->socketId = $this->getConnId();                        
        $connection->app = $this->app;   
        $connection->app->newConn($connection);
    }
    
    public function onClose(ConnectionInterface $connection) 
    {                
        $connection->app->leftChat($connection);
    }
    
    public function onError(ConnectionInterface $connection, \Exception $e)
    {
        
    }
    
    public function onMessage(ConnectionInterface $connection, MessageInterface $msg) 
    {        
        $connection->app->dialog($connection, $msg);
    }   
    
    private function getConnId() 
    {
        if($this->onePartConnId == 9999999999){
            $this->twoPartConnId++;
        }
        
        if($this->onePartConnId < 9999999999){
            $this->onePartConnId ++;
        }
        
        return str_pad($this->onePartConnId, 10, '0').'.'.str_pad($this->twoPartConnId, 10, '0');
    }    
}


