<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom;

/**
 * Description of Counting
 *
 * @author Evgeniy
 */
class Counting 
{    
    private $accumSumTotal = [];    
    private $accumSumGroup = [];    
    private $originalArray = [];

    public function countTotalGroup(array $array, array $sumKey) 
    {        
        if(empty($array)){
            return $array;
        }
        
        $this->originalArray = $array;                
                        
        $this->addLastEmptyElem($sumKey);
        
        foreach ($sumKey as $level => $fieldGroup) {            
            $this->countField($fieldGroup);            
        }
        
        $this->compliteTotal();         
        
        return $this->originalArray;      
    }    
    
    private function addLastEmptyElem(array $sumKey) 
    {
        $findArray = new \stdClass;
        foreach ($sumKey as $level => $fieldGroup) {            
            $findArray->$fieldGroup = '';
        }
        
        array_push($this->originalArray, $findArray);
    }
    
    private function countField(string $field) 
    {       
        $this->accumSumTotal = [];
        foreach ($this->originalArray as $key => $item) {            
            $prevKey = $key > 0 ? $key-1 : 0;            
            $prev = $this->originalArray[$prevKey];   
            
            if($prev->$field !== $item->$field){
                $key = 'total_'.$field;
                $this->originalArray[$prevKey]->$key = $this->accumSumGroup;
                $this->accumSumGroup = [];
            }
            
            $this->accumValue($item);
        }
    }
    
    private function accumValue(\stdClass $item) 
    {        
        foreach ($item as $fieldName => &$fieldValue) {            
            if (gettype($fieldValue) == 'double' or gettype($fieldValue) == 'NULL') {
                $fieldValue = round($fieldValue, 2);
                $valueGroup = array_key_exists($fieldName, $this->accumSumGroup) ? $this->accumSumGroup[$fieldName] + $fieldValue : $fieldValue;
                $valueTotal = array_key_exists($fieldName, $this->accumSumTotal) ? $this->accumSumTotal[$fieldName] + $fieldValue : $fieldValue;
                $this->accumSumGroup[$fieldName] = $valueGroup;
                $this->accumSumTotal[$fieldName] = $valueTotal;
            } 
        }        
    }
    
    private function compliteTotal() 
    {
        $lastIndex = array_key_last($this->originalArray);
        unset($this->originalArray[$lastIndex]);
        $this->originalArray['total'] = $this->accumSumTotal;
    }
}
