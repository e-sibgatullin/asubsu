<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom\SocialAuth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\SocialiteManager;
use App\Custom\SocialAuth\AuthVK;
/**
 * Description of ExtendSocialAuth
 *
 * @author Evgeniy
 */
class ExtendSocialAuth extends SocialiteManager
{    
    protected function createVkontakteDriver()
    {
        $config = $this->app->make('config')['services.vkontakte'];

        return $this->buildProvider(
            AuthVK::class, $config
        );
    }
}
