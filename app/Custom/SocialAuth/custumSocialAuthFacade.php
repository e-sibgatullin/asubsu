<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom\SocialAuth;
use Illuminate\Support\Facades\Facade;
use App\Custom\SocialAuth\ExtendSocialAuth;
/**
 * Description of custumSocialAuthFacade
 *
 * @author Evgeniy
 */
class custumSocialAuthFacade extends Facade
{
    protected static function getFacadeAccessor()
    {        
        return ExtendSocialAuth::class;
    }
}
