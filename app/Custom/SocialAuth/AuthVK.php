<?php
namespace App\Custom\SocialAuth;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthVK
 *
 * @author Evgeniy
 */

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthVK extends AbstractProvider implements ProviderInterface
{
    protected $authURL = "https://oauth.vk.com/authorize/";
    protected $accessTokenURL = "https://oauth.vk.com/access_token/";
    protected $apiURL = "https://api.vk.com/method/";
            
    CONST VERSION_API = "5.100";    
      
    public function redirect() 
    {
        $this->setScopes("email");
        return parent::redirect();
    }
    
    public function user() 
    {
        if ($this->hasInvalidState()) {
            throw new InvalidStateException;
        }

        $response = $this->getAccessTokenResponse($this->getCode());
        
        $user = $this->mapUserToObject($this->getUser($response));
        
        return $user->setToken(Arr::get($response, 'access_token'))                    
                    ->setExpiresIn(Arr::get($response, 'expires_in'));
    }
    
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase($this->authURL, $state);
    }
    
    protected function getTokenUrl()
    {        
        $state = $this->request->input('state');
        return $this->buildAuthUrlFromBase($this->accessTokenURL, $state);
    }
    
    protected function getUserByToken($token)
    {
       
    }
    
    protected function getUser(array $userData)
    {
        $userUrl = $this->apiURL.'users.get';
        
        $params = [
            "headers" => [
                "Accept" => "application/json",
            ],
            "form_params" => [
                "user_ids" => Arr::get($userData, 'user_id'),
                "fields" => "photo_100,bdate,nickname",                
                "v" => self::VERSION_API,
                "access_token" =>  Arr::get($userData, 'access_token'),
            ],
        ];

        $response = $this->getHttpClient()->post(
            $userUrl, $params
        );

        $responseArray = json_decode($response->getBody(), true);       
        $user = Arr::first($responseArray);
        $user[0]['email'] = Arr::get($userData, 'email');        
        
        return $user;
    }

    protected function mapUserToObject(array $user)
    {   
        $user = Arr::first($user);        
        return (new User)->setRaw($user)->map([
            'id' => $user['id'],
            'nickname' => $user['nickname'],
            'name' => $user['first_name'],
            'email' => $user['email'],
            'avatar' => $user['photo_100'],
        ]);
    }    
    
    protected function getUserInfo(string $fields = "bdate,photo_50", string $nameCase = "nom") 
    {
        $url = $this->apiURL."users.get?";        
        $params['fields'] = $fields;
        $params['name_case'] = $nameCase;        
        
    }    
}
