<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom\Concrete;

/**
 * Description of DeviceFabricate
 *
 * @author Evgeniy
 */
use \Modbus\ModbusTCP;
use Illuminate\Database\Eloquent\Model;

class DeviceFabricate 
{
    static public function build(ModbusTCP $modbus, Model $devModel) 
    {
        $namespace = '\\App\Custom\Concrete\\';
        $devTypeName = class_basename($devModel);
        $DevClass = $namespace.$devTypeName;
        
        return new $DevClass($modbus, $devModel);        
    }
}
