<?php
namespace App\Custom\Concrete;

use \Modbus\ModbusTCP;
use \Modbus\PacketBuilder;
use Illuminate\Database\Eloquent\Model;

class Mixer extends BaseDev
{   
    private $startPort;
    private $IPort;
    private $startPortState = self::PORT_CLOSE;

    public function __construct(ModbusTCP $modbus, Model $model) 
    {
        parent::__construct($modbus, $model);
        $this->startPort = $model->startPort;
        $this->IPort = $model->amperPort;        
    }    
    
    public function start() 
    {
        $this->modbus                
                ->fc(PacketBuilder::FC5)
                ->startReg($this->startPort)
                ->valReg(self::PORT_OPEN);
        $this->modbus->send();
        $this->startPortState = self::PORT_OPEN;
    }
    
    public function stop() 
    {
        $this->modbus                
                ->fc(PacketBuilder::FC5)
                ->startReg($this->port['startPort'])
                ->valReg(self::PORT_CLOSE);
        $this->modbus->send();
        $this->startPortState = self::PORT_CLOSE;
    }
    
    public function curentState() 
    {        
        $this->modbus                
                ->dataTYpe([PacketBuilder::DOUBLE])
                ->fc(PacketBuilder::FC3)
                ->startReg($this->port['IPort']);
        $response = $this->modbus->send()->getResponse();              

        return [
            'IPort' => array_shift($response), 
            'start' => $this->startPortState,
            'open' => $this->openPortState,
            'id' => $this->devId
            ];
    }    
}
