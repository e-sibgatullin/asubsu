<?php
namespace App\Custom\Concrete\ProcesStage;

use App\Custom\Interfaces\ConcreteStageInterface;
use App\Models\Recipe;
use App\Custom\Concrete\ProcesStage\Dosage;
use App\Exceptions\ConcreteException;
use App\Custom\Concrete\DeviceFabricate;

class Start implements ConcreteStageInterface
{
    CONST DEFAULT_QUANTITY = 1;
    
    private $concrete;
    private $workRecipe; 
    private $workQuantity;    

    public function __construct($concrete) 
    {
        $this->concrete = $concrete;
    }    
    
    public function run() 
    {                
        $this->setWorkRecipe()->fillStorageDevToWork();        
        $this->concrete->sendMsg('Рецепт ' . $this->workRecipe->shortName . ' запущен в работу.');        
    }
    
    private function setWorkRecipe() 
    {
        $recipeID           = key($this->concrete->getFirstRecipeFromQueue());          
        $this->workRecipe   = Recipe::find($recipeID);                              
        if(!$this->workRecipe){
            throw new ConcreteException('У рецепта ' . $this->workRecipe->shortName . ' не найдено компонентов.');              
        }
        
        return $this;
    }
    
    private function fillStorageDevToWork() 
    {        
        $storageDev = $this->workRecipe->components()->get()->map(function($item){               
            $hopperModel            = $item->hoppers()->first();
            $hopperDev              = $this->concrete->getDev($item->hoppers()->first());
            $dispenserDev           = $this->concrete->getDev($hopperModel->dispenser()->first());
            $quantity               = current($this->concrete->getFirstRecipeFromQueue());
            $this->workQuantity     = min($quantity, self::DEFAULT_QUANTITY);            
            $massComp               = $item->pivot->m *$this->workQuantity;
            $dispenserDev->setLimitMass($massComp);
            return [
                'compID' => $item->id, 
                'compM' => $massComp, 
                'hopper' => $hopperDev, 
                'dispenser' => $dispenserDev
                    ];            
        })->toArray();
        
        $this->concrete->setQuantityProc($this->workQuantity);
        $this->concrete->setStorageDev($storageDev);        
        $this->openHopper($storageDev);
    }    
    
    private function openHopper(array $storageDev) 
    {
        foreach ($storageDev as $compLine) {            
            $hopperDev = $compLine['hopper'];
            $hopperDev->open();
        }        
        
        $this->concrete->changeStage(new Dosage($this->concrete));
    }      
}
