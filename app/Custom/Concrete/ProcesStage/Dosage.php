<?php

namespace App\Custom\Concrete\ProcesStage;

use App\Custom\Interfaces\ConcreteStageInterface;
use App\Custom\Concrete\ProcesStage\Start;
/**
 * Description of Dosage
 *
 * @author Evgeniy
 */
class Dosage implements ConcreteStageInterface
{
    private $concrete;    
    private $storageDev;
    private $numberOfEmptyDispensers = 0;

    public function __construct($concrete) 
    {
        $this->concrete = $concrete;
        $this->storageDev = $this->concrete->getStorageDev();
    }
    
    public function run() 
    {        
        $this->numberOfEmptyDispensers = 0;
        foreach ($this->storageDev as $compLine) {
            $this->checkDosage($compLine);
        }  
                
        if($this->numberOfEmptyDispensers >= count($this->storageDev)){            
            $this->concrete->decQuantityQueue();                       
            $this->concrete->changeStage(new Start($this->concrete));            
        }
        
        $this->concrete->sendMsg($this->concrete->createResponseData($this->storageDev));           
    } 

    private function checkDosage(array $dev)
    {
        $dispenserDev = $dev['dispenser'];
        $hopperDev = $dev['hopper'];
        if ($dispenserDev->isEnough()) {
            $hopperDev->close();
            $dispenserDev->open();
        }

        if ($dispenserDev->openPortState() && $dispenserDev->isEmpty()) {
            $dispenserDev->close();
        }

        if ($dispenserDev->isEmpty() && !$hopperDev->curentState()['state']) {
            $this->numberOfEmptyDispensers++;
        }
    }
}
