<?php
namespace App\Custom\Concrete;

use \Modbus\ModbusTCP;
use \Modbus\PacketBuilder;
use Illuminate\Database\Eloquent\Model;

class Dispenser extends BaseDev
{    
    private $maxMass; 
    private $factMass;    
    private $limitMass;
    private $mPort;

    public function __construct(ModbusTCP $modbus, Model $model) 
    {        
        parent::__construct($modbus, $model);
        $this->mPort = $model->massPort;        
        $this->maxMass = $model->maxMass;
        $this->limitMass = $this->maxMass;        
    }    
    
    public function open() 
    {
        $this->factMass = $this->getCurrentMass();
        parent::open();
    }
    
    public function curentState() 
    {        
        return [
            'openPort' => $this->openPortState(), 
            'currentMass' => $this->getCurrentMass(),
            'id' => $this->devId
                ];
    }    
    
    public function isEnough() 
    {        
        $limitMass = min($this->maxMass, $this->limitMass);
        if($this->getCurrentMass() >= $limitMass){            
            return true;
        }
        
        return false;
    }
    
    public function setLimitMass($mass) 
    {
        if(empty($mass)){
            return;
        }
        
        $this->limitMass = $mass;
    }
    
    public function isEmpty()
    {        
        return $this->getCurrentMass() <= 0 ? true : false;
    }
    
    public function getFactMass() 
    {
        return $this->factMass ?: 0;
    }
    
    private function getCurrentMass() 
    {
        $this->modbus                
                ->fc(PacketBuilder::FC3)
                ->dataTYpe([PacketBuilder::DOUBLE])
                ->startReg($this->mPort);
        $responseData = $this->modbus->send()->getResponse();        
        return array_shift($responseData);
    }  
    
    public function openPortState()
    {
        return $this->openPortState;
    }
}
