<?php
namespace App\Custom\Concrete;

use \App\Custom\Interfaces\ConcreteStageInterface;
use \App\Custom\Concrete\ProcesStage\Start;
use \App\Exceptions\ConcreteException;
use \Modbus\ModbusTCP;
use Ratchet\ConnectionInterface;
use BeyondCode\LaravelWebSockets\Apps\App;

class Concrete extends App
{
    CONST AUTO              = 1;
    CONST MANUAL            = 0;
    CONST MSG_STATE_INFO    = 'info';
    CONST MSG_STATE_ERROR   = 'error';    
        
    protected $recipe;
    protected $modbus;     
    protected $wsConnection;    
    protected $quantityProcces;
    
    private $stage;
    private $storageDev;

    public function __construct(ModbusTCP $modbus) 
    {        
        $this->modbus = $modbus;   
        $this->recipe = new \SplDoublyLinkedList();

        $appAttributes = collect(config('websockets.apps'))->first();      
        $this->changeStage(new Start($this));        
        
        if (isset($appAttributes['name'])) {
            $this->setName($appAttributes['name']);
        }

        if (isset($appAttributes['host'])) {
            $this->setHost($appAttributes['host']);
        }

        if (isset($appAttributes['path'])) {
            $this->setPath($appAttributes['path']);
        }
        
        parent::__construct($appAttributes['id'], $appAttributes['key'], $appAttributes['secret']);
    }
    
    public function make($mode = self::AUTO, $recipe = null, $quantity = null) 
    {        
        if(!empty($recipe)){
            $this->addQueue($recipe, $quantity);
        }        
        
        if($this->recipe->isEmpty()){            
            throw new ConcreteException("Нет рецептов для изготовления бетона.");            
        }        
        
        $this->stage->run();       
    }  
    
    public function changeStage(ConcreteStageInterface $stage) 
    {
        $this->stage = $stage;
    }
    
    public function addQueue($recipe, $quantity = Start::DEFAULT_QUANTITY) 
    {        
        $msg        = "Рецепт $recipe добавлен в очередь с количеством кубов $quantity";
        $data       = [$recipe => $quantity];
        $this->recipe->push($data);        
        $this->sendMsg($msg);        
    }       
        
    public function decQuantityQueue() 
    {
        $recipeSplIndex     = $this->recipe->key();
        $recipeQuantity     = current($this->recipe->bottom());        
        $recipeID           = key($this->recipe->bottom());        
        $recipeQuantity     = $recipeQuantity - $this->quantityProcces;
        $this->recipe->offsetSet($recipeSplIndex, [$recipeID => $recipeQuantity]);
        if($recipeQuantity == 0){
            $this->recipe->shift();            
        }
    }        
    
    public function manualOpen($devType, $devID, $devState) 
    {        
        $devModelPath       = '\\App\Models\\'.$devType;
        $devModel           = $devModelPath::find($devID);
        $dev                = $this->getDev($devModel);        
        $method             = $devState ? 'open' : 'close';
        $dev->$method();
        $data[] = [$devType => $dev->curentState()];        
        $this->sendMsg($data);                
    }    
      
    public function createResponseData(array $devices) 
    {
        $result = [];
        foreach ($devices as $key => $device) {            
            if(is_array($device)){
                $result[$key] = $this->createResponseData($device);
                continue;
            }
            
            if(is_object($device)){
                $result[$key] = $device->curentState();
            }            
        }
        
        return $result;
    }

    public function sendMsg($msg, string $state = self::MSG_STATE_INFO) 
    {
        $msg = json_encode([            
            'state' => $state,             
            'data' => $msg,            
            'queue' => $this->getQueueInArray()]);
        
        $this->wsConnection->send($msg);                
    }   
    
    public function setWsConn(ConnectionInterface $conn) 
    {        
        $this->wsConnection = $conn;
    }     
    
    public function reconnModbus() 
    {
        $this->modbus->reconn();        
        $this->sendMsg('reconnection attempt.'); 
    }
    
    public function setStorageDev(array $dev) 
    {
        $this->storageDev = $dev;
    }
    
    public function setQuantityProc($quantity) 
    {
        $this->quantityProcces = $quantity;
    }
    
    public function getQueueInArray() 
    {
        $queueAray = [];
        foreach ($this->recipe as $key => $value) {
            $queueAray[] = $value;
        }
        
        return $queueAray;
    }
    
    public function getDev($devModel) 
    {
        return DeviceFabricate::build($this->modbus, $devModel);
    }
    
    public function getFirstRecipeFromQueue() 
    {
        return $this->recipe->bottom();
    }    
    
    public function getStorageDev() 
    {
        return $this->storageDev;
    }
    
    public function __call($method, $params) 
    {
        throw new ConcreteException("call to undefined method $method");    
    }
}
