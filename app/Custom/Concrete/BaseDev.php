<?php

namespace App\Custom\Concrete;

use \Modbus\ModbusTCP;
use \Modbus\PacketBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Custom\Interfaces\DeviceInterface;

class BaseDev implements DeviceInterface
{
    CONST PORT_CLOSE = 0;
    CONST PORT_OPEN = 1;
    
    protected $modbus;
    protected $name;
    protected $openPort;     
    protected $devId;
    protected $openPortState = self::PORT_CLOSE;
    
    public function __construct(ModbusTCP $modbus, Model $model)
    {
        $this->modbus = $modbus;
        $this->name = $model->name;
        $this->openPort = $model->openPort;
        $this->devId = $model->id;
    } 
    
    public function open()
    {
        $this->modbus                
                ->fc(PacketBuilder::FC5)
                ->startReg($this->openPort)
                ->valReg(self::PORT_OPEN);
        $this->modbus->send();
        $this->openPortState = self::PORT_OPEN;
    }
    
    public function close()
    {
        $this->modbus                
                ->fc(PacketBuilder::FC5)
                ->startReg($this->openPort)
                ->valReg(self::PORT_CLOSE);
        $this->modbus->send();
        $this->openPortState = self::PORT_CLOSE;
    }    
}
