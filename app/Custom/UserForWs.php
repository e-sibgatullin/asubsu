<?php
namespace App\Custom;

use Illuminate\Support\Facades\Auth;
use Illuminate\Session\SessionManager;
use App\Models\User;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Support\Facades\Crypt;

class UserForWs 
{
    private $connection;
    private $sessionID;
    private $userID;
    
    public function __construct($connection)
    {
        $this->connection = $connection;
        $this->sessionID = $this->getSessionID();   
        $this->userID = $this->getUserID();
    }
    
    public function isAuth() 
    {        
        return User::find($this->userID) ? true : false;
    }
    
    public function getUser() 
    {
        if(!empty($this->userID)){
            return User::find($this->userID);
        }
        
        return $this->createUser();
    }
    
    private function getSessionID() 
    {
        $cookiesStr = $this->connection->httpRequest->getHeaderLine('Cookie');
        $cookieMath = array_filter(explode(';', $cookiesStr), function($cookie){
            return strripos($cookie, config('session.cookie')) === false ? false : true;
        }); 
        
        $cookie = SetCookie::fromString(implode($cookieMath));                        
        return Crypt::decrypt(urldecode($cookie->getValue()), false);
    }
    
    private function getUserID()
    {
        if(empty($this->sessionID)) return false;
        
        $userIdKey = Auth::getName();
        $session = (new SessionManager(app()))->driver();
        $session->setId($this->sessionID);
        $session->start();
        
        if(!$session->has($userIdKey)) return false;        
        
        return $session->get($userIdKey);                                  
    }    
    
    private function createUser() 
    {        
        return User::create([
            'id' => $this->connection->socketId,
            'name' => 'guest',
            'email' => 'guest@guest.gu',
        ]);
    }
}
