<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Custom;

/**
 * Description of ConnectSteaming
 *
 * @author Evgeniy
 */
class ConnectSteaming 
{
    private $socket;
    private $response;
    private $host;
    private $port;
    public $connected = false;

    public function __construct($host = false, $port = false) {
        $this->host = empty($host) ? '10.0.3.200' : $host;
        $this->port = empty($port) ? '8000' : $port;
        $this->socketCreate();
    }

    private function socketCreate() {
        $this->socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option(
                $this->socket,
                SOL_SOCKET,
                SO_RCVTIMEO,
                ['sec' => 5, 'usec' => 0]
        );
        socket_set_option(
                $this->socket,
                SOL_SOCKET,
                SO_SNDTIMEO,
                ['sec' => 5, 'usec' => 0]
        );
        
        $this->connect();
    }

    private function connect() {
        if (false === @socket_connect($this->socket, $this->host, $this->port)) {                        
            $this->connected = false;
            $msg = socket_strerror(socket_last_error()). " $this->host $this->port";
            throw new \App\Exceptions\ConnectSteamingException($msg);
        }
        $this->connected = true;
    }
    
    public function send($msg) 
    {
        if(!$this->connected){
            return false;
        }
        
        if(@socket_write($this->socket, $msg.PHP_EOL) === false){            
            $this->connected = false;
            throw new \App\Exceptions\ConnectSteamingException(socket_strerror(socket_last_error()));             
        } 
        
        $this->read();
        return $this;
    }
    
    private function read() 
    {
        $this->response = @socket_read($this->socket, 2000);
        if($this->response === false){            
            $this->connected = false;
            throw new \App\Exceptions\ConnectSteamingException(socket_strerror(socket_last_error()));             
        }
        return true;
    }
    
    public function getResponse() 
    {        
        return json_decode($this->response);
    }

    public function close() 
    {
        socket_set_option(
                $this->socket,
                SOL_SOCKET,
                SO_LINGER,
                ['l_onoff' => 1, 'l_linger' => 0]);
        socket_close($this->socket);
    }
}
