<?php

namespace App\Exceptions;
use Illuminate\Support\Facades\Log;

use Exception;

class ConnectSteamingException extends Exception
{
    public function render($request) 
    {        
        $message = iconv('CP1251', 'utf-8', $this->getMessage()) ?? $this->getMessage();   
        $dataError = ['status'=>'error','message'=> $message];
        return view('pages.steaming',['title'=>':: Пропарка', 'facilitys'=> $dataError]);
        //return response()->json(['status'=>'error','data'=> $message]);        
    }
}
