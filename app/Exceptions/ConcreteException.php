<?php

namespace App\Exceptions;

use Exception;

class ConcreteException extends Exception
{
    public function render($request) 
    {        
        return $this->getMessage();           
    }
}
