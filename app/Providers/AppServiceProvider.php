<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {        
        $this->app->bind('Counting', function(){
            return new \App\Custom\Counting();
        });
        
        $this->app->bind('ReportXML', function(){
            return new \App\Custom\CreateXML(public_path());
        });                
        
        $this->app->bind('Modbus\Interfaces\ParserInterface', '\Modbus\ModbusParser');
        $this->app->bind('Modbus\Interfaces\PacketBuilderInterface', '\Modbus\PacketBuilder');        
        $this->app->bind('modbus', \Modbus\ModbusTCP::class);
        
        $this->app->bind('ConcreteLine1', function(){
            app()->when(\Modbus\ModbusTCP::class)->needs('$setings')->give(config('modbus.line1'));
            return new \App\Custom\Concrete\Concrete(resolve('modbus'));
        });                            
        
        $this->app->bind('ConcreteLine2', function(){
            app()->when(\Modbus\ModbusTCP::class)->needs('$setings')->give(config('modbus.line2'));
            return new \App\Custom\Concrete\Concrete(resolve('modbus'));
        });                            
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
