<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsLinkHoppersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('componentsLinkHoppers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('compID');
            $table->foreign('compID')->references('id')->on('components');
            $table->bigInteger('hopperID')->unsigned();            
            $table->foreign('hopperID')->references('id')->on('hoppers');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('componentsLinkHoppers');
    }
}
