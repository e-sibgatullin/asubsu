<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispancersLinkMixerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensersLinkMixer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('mixerID')->unsigned();
            $table->bigInteger('dispencerID')->unsigned();
            $table->foreign('mixerID')->references('id')->on('mixers');
            $table->foreign('dispenserID')->references('id')->on('dispensers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispensersLinkMixer');
    }
}
