<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMixerLinkOptDevTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mixerLinkOptDev', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('mixerID')->unsigned();
            $table->foreign('mixerID')->references('id')->on('mixers');
            $table->bigInteger('optDevID')->unsigned();            
            $table->foreign('optDevID')->references('id')->on('optionalDev');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mixerLinkOptDev');
    }
}
