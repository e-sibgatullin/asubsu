<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoppersLinkDispancersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoppersLinkDispensers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hopperID')->unsigned();
            $table->bigInteger('dispenserID')->unsigned();
            $table->foreign('hopperID')->references('id')->on('hoppers');
            $table->foreign('dispenserID')->references('id')->on('dispensers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoppersLinkDispensers');
    }
}
