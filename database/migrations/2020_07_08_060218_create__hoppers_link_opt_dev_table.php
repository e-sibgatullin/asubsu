<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoppersLinkOptDevTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoppersLinkOptDev', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hopperID')->unsigned();
            $table->bigInteger('optDevID')->unsigned();
            $table->foreign('hopperID')->references('id')->on('hoppers');
            $table->foreign('optDevID')->references('id')->on('optionalDev');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoppersLinkOptDev');
    }
}
